/*----------------------------- Include Files -----------------------------*/
//include header files for this service
#include "Basket.h"

//include header files for hardware access
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

//Header for Timing Functions
#include "ES_Port.h"

//include header files for the framework
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"
#include "ES_ShortTimer.h"

#include "termio.h"

//include header files for the other modules that are referenced

//Include Port functions
#include "Ports.h"

/*----------------------------- Module Defines ----------------------------*/
//Basket beam break is PA2
#define BEAMBREAK_PIN GPIO_PIN_2
#define BEAM_LO  BIT2LO
#define BEAM_HI  BIT2HI

//Basket lights are PB3
#define BASKETLIGHTS_PIN GPIO_PIN_3
#define BASKETLIGHTS_LO  BIT3LO
#define BASKETLIGHTS_HI  BIT3HI

#define BASKET_TIMER 9 

#define WELCOME_TIME 1000
#define BASKET_TIME 1000.0
#define BASKET_SWITCH_TIME 200
#define CELEBRATION_TIME 200


/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
relevant to the behavior of this service. */

/*---------------------------- Module Variables ---------------------------*/
//-with the introduction of Gen2, we need a module level Priority variable
//-Data private to the module:
static uint8_t MyPriority;
static BasketState_t CurrentState;
static char LastInputState;
static uint8_t Counter;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
Function:       InitMorseElements
Parameters:     uint8_t : the priorty of this service
Returns:        bool, false if error in initialization, true otherwise
Description:    Saves away the priority, and does any other required initialization
                for this service
****************************************************************************/

bool InitializeBasket (uint8_t Priority)
{
  ES_Event_t ThisEvent;
  MyPriority = Priority;

  //Initialize the port line to receive beam break signals
  SetPin2Digital_PortA(BEAMBREAK_PIN);
  SetPin2Input_PortA(BEAMBREAK_PIN);
  
  //Initialize the port line to send basket light signals
  SetPin2Digital_PortB(BASKETLIGHTS_PIN);
  SetPin2Output_PortB(BASKETLIGHTS_PIN);
  
  //Sample port line and use it to initialize the LastInputState variable
  LastInputState = (PORTA & BEAMBREAK_PIN);

  //Set CurrentState to be InitBasket
  CurrentState = InitBasket;

  //Post Event ES_Init to MorseElements queue (this service)
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService( MyPriority, ThisEvent) == true)
  {
    return true;
  }else
  {
    return false;
  }
}

/****************************************************************************
Function:     PostLCDService
Parameters:   EF_Event ThisEvent ,the event to post to the queue
Returns:      bool false if the Enqueue operation failed, true otherwise
Description:  Posts an event to this state machine's queue
Author:       J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostBasket (ES_Event_t ThisEvent)
{
  return ES_PostToService( MyPriority, ThisEvent);
}

/****************************************************************************
Function:     RunBasket
Parameters:   ES_Event_t : the event to process
Returns:      ES_Event_t, ES_NO_EVENT if no error ES_ERROR otherwise
Description:  State Machine for basket
****************************************************************************/
ES_Event_t RunBasket (ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
  BasketState_t NextState = CurrentState; //Initialize NextState to CurrentState

  switch (CurrentState)
  {
    case InitBasket :
      if ( ThisEvent.EventType == ES_INIT )
      {
        printf("\rBasket Starting\n\r");
        //Set state to WelcomeLights off
        NextState = WelcomeLightsOff;
        //Set timer for basket lights
        ES_Timer_InitTimer(BASKET_TIMER, WELCOME_TIME);
      }
      break;
    case WelcomeLightsOff :
      //If the master SM sends a GAME_START event, switch to PlayingLightsOn
      if ( ThisEvent.EventType == GAME_START )
      {
        //Stop timer
        ES_Timer_StopTimer(BASKET_TIMER);
        //Change to playing state
        printf("\rChange to playing\n\r");
        NextState = PlayingLightsOff;
      }
      //if we have a timeout Event
      if( ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == BASKET_TIMER)
      {
        //Turn Lights on
        printf("\rTurning Lights on\n\r");
        PORTB |= BASKETLIGHTS_HI;
        //Reset Timer
        ES_Timer_InitTimer(BASKET_TIMER, WELCOME_TIME);
        //Switch to WelcomeLightsOn
        NextState = WelcomeLightsOn;
      }
      break;
    case WelcomeLightsOn :
      //If the master SM sends a GAME_START event, switch to PlayingLightsOn
      if ( ThisEvent.EventType == GAME_START )
      {
        printf("\rChange to playing\n\r");
        //Stop timer
        ES_Timer_StopTimer(BASKET_TIMER);
        //Change to playing state off
        NextState = PlayingLightsOff;
        //Turn lights off
        PORTB &= BASKETLIGHTS_LO;
      }
      //Timeout Event
      if( ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == BASKET_TIMER)
      {
        //Turn Lights off
        printf("\rTurning Lights off\n\r");
        PORTB &= BASKETLIGHTS_LO;
        //Reset Timer
        ES_Timer_InitTimer(BASKET_TIMER, WELCOME_TIME);
        //Switch to WelcomeLightsOff
        NextState = WelcomeLightsOff;
      }
      break;
    case PlayingLightsOff :
      //If a BASKET_MADE event is detected
      if ( ThisEvent.EventType == BASKET_MADE )
      {
        printf("\rBasket Made\n\r");
        //Send a point to point SM

        //Set Counter variable
        Counter = BASKET_TIME/BASKET_SWITCH_TIME;
        //Set Timer
        ES_Timer_InitTimer(BASKET_TIMER, BASKET_SWITCH_TIME);
        //Preserve current state
        NextState = PlayingLightsOff;
      }
      if( ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == BASKET_TIMER)
      {
        //If the counter is equal to zero, we stop flashing
        //(ie don't turn lights or timer back on)
        if(Counter == 0)
        {
          NextState = PlayingLightsOff;
        }
        //Otherwise if counter is greater than zero, we flash
        //and decrement the counter
        else if (Counter > 0)
        {
          //Decrement the counter
          Counter--;
          //Set the timer again
          ES_Timer_InitTimer(BASKET_TIMER, BASKET_SWITCH_TIME);
          //Turn the Lights on
          PORTB |= BASKETLIGHTS_HI;
          //Change state variable
          NextState = PlayingLightsOn;
        }
      }
      //If the master SM sends a RESET event, switch back to Welcome mode
      if ( ThisEvent.EventType == RESET )
      {
        //Change back to welcome mode
        NextState = WelcomeLightsOff;
        //Set timer for welcome mode flashing
        ES_Timer_InitTimer(BASKET_TIMER, WELCOME_TIME);
      }
      //If the master SM sends a GAME_OVER event, switch to celebration mode
      if ( ThisEvent.EventType == GAME_OVER )
      {
        //Change to Celebration mode
        NextState = CelebrationLightsOff;
        //Set timer for celebration flashing
        ES_Timer_InitTimer(BASKET_TIMER, CELEBRATION_TIME);
      }
      break;
    case PlayingLightsOn :
      //If a BASKET_MADE event is detected
      if ( ThisEvent.EventType == BASKET_MADE )
      {
         printf("\rBasket Made\n\r");
        //Send a point to point SM

        //Set Counter variable
        Counter = BASKET_TIME/BASKET_SWITCH_TIME;
        //Set Timer
        ES_Timer_InitTimer(BASKET_TIMER, BASKET_SWITCH_TIME);
        //Preserve current state
        NextState = PlayingLightsOn;
      }
      if( ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == BASKET_TIMER)
      {
        //If the counter is equal to zero, we stop flashing
        //(ie we turn lights off and don't turn timer back on)
        if(Counter == 0)
        {
          //Turn lights off
          PORTB &= BASKETLIGHTS_LO;
          //Change state
          NextState = PlayingLightsOff;
        }
        //Otherwise if counter is greater than zero, we flash
        //and decrement the counter
        else if (Counter > 0)
        {
          //Decrement the counter
          Counter--;
          //Set the timer again
          ES_Timer_InitTimer(BASKET_TIMER, BASKET_SWITCH_TIME);
          //Turn the Lights off
          PORTB &= BASKETLIGHTS_LO;
          //Change state variable
          NextState = PlayingLightsOff;
        }
      }
      //If the master SM sends a RESET event, switch back to Welcome mode
      if ( ThisEvent.EventType == RESET )
      {
        //Turn lights off
        PORTB &= BASKETLIGHTS_LO;
        //Change back to welcome mode
        NextState = WelcomeLightsOff;
        //Set timer for welcome mode flashing
        ES_Timer_InitTimer(BASKET_TIMER, WELCOME_TIME);
      }
      //If the master SM sends a GAME_OVER event, switch to celebration mode
      if ( ThisEvent.EventType == GAME_OVER )
      {
        //Turn Lights off
        PORTB &= BASKETLIGHTS_LO;
        //Change to Celebration mode
        NextState = CelebrationLightsOff;
        //Set timer for celebration flashing
        ES_Timer_InitTimer(BASKET_TIMER, CELEBRATION_TIME);
      }
      break;
    case CelebrationLightsOff :
      if ( ThisEvent.EventType == RESET )
      {
        //Change back to welcome mode
        NextState = WelcomeLightsOff;
        //Set timer for welcome mode flashing
        ES_Timer_InitTimer(BASKET_TIMER, WELCOME_TIME);
      }
      if( ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == BASKET_TIMER)
      {
        printf("\rCelebration mode\n\r");
        //Turn lights on
        PORTB |= BASKETLIGHTS_HI;
        //Set timer for celebration mode flashing
        ES_Timer_InitTimer(BASKET_TIMER, CELEBRATION_TIME);
        //Set next state
        NextState = CelebrationLightsOn;
      }
      break;
    case CelebrationLightsOn :
      if ( ThisEvent.EventType == RESET )
      {
        //Turn lights off
        PORTB &= BASKETLIGHTS_LO;
        //Change back to welcome mode
        NextState = WelcomeLightsOff;
        //Set timer for welcome mode flashing
        ES_Timer_InitTimer(BASKET_TIMER, WELCOME_TIME);
      }
      if( ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == BASKET_TIMER)
      {
        //Turn lights off
        PORTB &= BASKETLIGHTS_LO;
        //Set timer for celebration mode flashing
        ES_Timer_InitTimer(BASKET_TIMER, CELEBRATION_TIME);
        //Set next state
        NextState = CelebrationLightsOff;
      }
      break;
  }
  CurrentState = NextState; //Update CurrentState
  return ReturnEvent;
}

/****************************************************************************
Function: 	  CheckBasket
Parameters: 	None
Returns: 	    bool: true if a new event was detected
Description:	Checks for falling and rising edges on the Basket Pins
Notes:
****************************************************************************/
bool CheckBasket(void)
{
  uint8_t         CurrentInputState;
  bool            ReturnVal = false; //Assume no event has occurred

  //Get the CurrentInputState from the input line
  CurrentInputState = (PORTA & BEAMBREAK_PIN);
   //printf("\rCurrentInputState %d\n\r",CurrentInputState);
  if (CurrentInputState != LastInputState)
  {
    ES_Event_t ThisEvent; //An event has occured
    //If the current state of the input line is high. Beam break goes from
    //low to high when beam is broken
    if(CurrentInputState != 0)
    {
      //A basket had been made. Post event to BasketSM
      ThisEvent.EventType = BASKET_MADE;
      PostBasket(ThisEvent);
    }
  ReturnVal = true;
  }
  LastInputState = CurrentInputState; // update the state for next time //[x]

return ReturnVal;
}

/*NOTES:
- BASKET_MADE event added

*/
/********************************END OF FILE**************************************/
