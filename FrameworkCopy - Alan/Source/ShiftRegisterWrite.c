/****************************************************************************
 Module
   ShiftRegisterWrite.c

 Revision
   1.0.1

 Description
   This module acts as the low level interface to a write only shift register.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 10/11/15 19:55 jec     first pass

****************************************************************************/
//Testing macro for test harness
//#define TEST

//Module for interfacing with Port B
#include "Ports.h"

//header for this c file
#include "ShiftRegisterWrite.h"

//Shift register uses PBO,PB1,PB2
#define DATA    GPIO_PIN_0
#define DATA_HI BIT0HI
#define DATA_LO BIT0LO

#define SCLK    GPIO_PIN_1
#define SCLK_HI BIT1HI
#define SCLK_LO BIT1LO

#define RCLK    GPIO_PIN_2
#define RCLK_LO BIT2LO
#define RCLK_HI BIT2HI

#define GET_MSB_IN_LSB(x) ((x & 0x80)>>7)

// an image of the last 8 bits written to the shift register
static uint8_t LocalRegisterImage=0;

/*Initializes Port B, sets pins 0,1,2 to be digital outputs, and sets the
initial states of the pins */
void SR_Init(void){

  // Manipulate port B setting the direction
  // of PB0, PB1 & PB2 to output
  //---------------------------------------------------------------

  //Set pins 0,1,2 on Port B to be digital
  SetPin2Digital_PortB( (DATA | SCLK | RCLK) );
  //Set pins 01,2, on Port B to be an output
  SetPin2Output_PortB( (DATA | SCLK | RCLK) );

  // start with the data & sclk lines low
  PORTB &= (DATA_LO & SCLK_LO);
  // and the RCLK line high
  PORTB |= RCLK_HI;

  //Intialize Shift Register Pins as LOW
  SR_Write(0);
}

/*Returns the last 8 bits written to the shift register */
uint8_t SR_GetCurrentRegister(void){
  return LocalRegisterImage;
}

/*Writes a given byte to the shift register */
void SR_Write(uint8_t NewValue){

  uint8_t BitCounter;
  LocalRegisterImage = NewValue; // save a local copy
  uint8_t Write = 0;

  // lower the register clock
  PORTB &= RCLK_LO;

  //shift out the data while pulsing the serial clock
  for(int i = 0; i<8; i++)
  {
    // Isolate the MSB of NewValue, put it into the LSB position of variable Write
    Write = GET_MSB_IN_LSB(NewValue);
    //Clear PB0 and then OR the port with Write to obtain the proper value on PB0
    PORTB = (PORTB & BIT0LO) | Write;
    //Shift NewValue bits over by one to prepare for next iteration
    NewValue = NewValue<<1;
    // raise SCLK
    PORTB |= SCLK_HI;
    // lower SCLK
    PORTB &= SCLK_LO;
  }
  // raise the register clock to latch the new data
  PORTB |= RCLK_HI;
}

#ifdef TEST

#include "termio.h"
int main(void)
{
  // initialize the timer sub-system and console I/O
	TERMIO_Init();
	//clrScrn();

	// When doing testing, it is useful to announce just which program
	// is running.
	puts("\n\rShiftRegisterWrite Test Harness\r");
	printf("%s %s\n",__TIME__, __DATE__);
	printf("\n\r\n");

  SR_Init();
  printf("\rInit done\n");

  uint8_t testByte = 12;
  SR_Write(testByte);
  printf("\rWrite done\n");


  uint8_t currentRegister = 0;
  currentRegister = SR_GetCurrentRegister();
  printf("\rCurrent Register is %d\n\r",currentRegister);

  return 0;
}

#endif
