/****************************************************************************
 Module
   TemplateFSM.c

 Revision
   1.0.1

 Description
   This is a template file for implementing flat state machines under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/15/12 11:12 jec      revisions for Gen2 framework
 11/07/11 11:26 jec      made the queue static
 10/30/11 17:59 jec      fixed references to CurrentEvent in RunTemplateSM()
 10/23/11 18:20 jec      began conversion from SMTemplate.c (02/20/07 rev)
 
 / TODO access to posting points to write to LCD/Screen
// TODO access to trigger specific sounds on audio board
****************************************************************************/


/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "Points.h"



/*----------------------------- Module Defines ----------------------------*/

#define BASKET_POINTSVAL 1
#define RAIN_POINTSVAL 2
#define LIGHT_POINTSVAL 3


/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this machine.They should be functions
   relevant to the behavior of this state machine
*/

/*---------------------------- Module Variables ---------------------------*/
// everybody needs a state variable, you may need others as well.
// type of state variable should match htat of enum in header file


// with the introduction of Gen2, we need a module level Priority var as well
static uint8_t MyPriority;

static uint32_t Points;	//stores the points 

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitPoints

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, sets up the initial transition and does any
     other required initialization for this state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 18:55
****************************************************************************/
bool InitPoints(uint8_t Priority)
{

  MyPriority = Priority;
  // put us into the Initial PseudoState
  
	//TODO: Initialize SoundBoard Hardware
  
}

/****************************************************************************
 Function
     PostPoints

 Parameters
     EF_Event ThisEvent , the event to post to the queue

 Returns
     boolean False if the Enqueue operation failed, True otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostPoints(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunPoints

 Parameters
   ES_Event : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes
   uses nested switch/case to implement the machine.
 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunPoints(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
	uint32_t NoOfPoints;
	
	if(ThisEvent.EventType == POST_POINTS)
	{
		NoOfPoints = ThisEvent.EventParam;
		Points = Points + NoOfPoints;		
		
		//POST TO LCD SM
		
		if(NoOfPoints == BASKET_POINTSVAL)
		{
			//TRIGGER BASKET BALL SOUND
		}
		if(NoOfPoints == RAIN_POINTSVAL)
		{
			//TRIGGER RAIN SOUND
		}
		if(NoOfPoints == LIGHT_POINTSVAL)
		{
			//TRIGGER LIGHT SOUND
		}
							
	}
	if(ThisEvent.EventType == RESET)
	{
		Points =0;
		//TRIGGER RELEVANT AUDIO FEEDBACK
	}
	
	
	
	
	
  return ReturnEvent;
}

/****************************************************************************
 Function
     GetPoints for other modules to access this

 Parameters
     None

 Returns
     Points uint32_t

****************************************************************************/
uint32_t GetPoints(void)
{
  return Points;
}

/***************************************************************************
 private functions
 ***************************************************************************/

