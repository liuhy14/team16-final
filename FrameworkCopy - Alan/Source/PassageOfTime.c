//#define TEST
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

// the headers to access the GPIO subsystem
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

// the headers to access the TivaWare Library
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"

#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_Timers.h"
#include "ES_Port.h"
#include "ES_Types.h"
#include "termio.h"
#include "EnablePA25_PB23_PD7_PF0.h"

#include "Master.h"
#include "PassageOfTime.h"
#include "Servo.h"
#include "ADMulti.h"



#define IcebergUpAngle 90 //135-90
#define TimeToRise 1000
#define IntervalBetweenFalls   4000
#define AngleChange -3

static uint8_t MyPriority;
static POTState_t CurrentState;
static uint8_t CurrentAngle;

bool InitPassageOfTime(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;	
	CurrentState = BeforeInit;   
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

bool PostPOT(ES_Event_t ThisEvent)
{
  return ES_PostToService( MyPriority, ThisEvent);
}

ES_Event_t RunPOT(ES_Event_t ThisEvent ){
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType  = ES_NO_EVENT;
  
  switch(CurrentState){
    case BeforeInit:
      if(ThisEvent.EventType == ES_INIT){
        printf("I get heure");
        ServoWrite(2,IcebergUpAngle);        
        ES_Timer_InitTimer(PassageOfTime_TIMER,TimeToRise);
        CurrentState = Raising;        
      }
      break;
    case Raising:
      if(ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == PassageOfTime_TIMER)
      {
        CurrentState = Welcome_POT;
        CurrentAngle = IcebergUpAngle;
      }
      break;
    case Welcome_POT:
      if(ThisEvent.EventType == GAME_START) 
      {
        CurrentState = Falling;
        ES_Timer_InitTimer(PassageOfTime_TIMER,IntervalBetweenFalls);
        CurrentAngle = CurrentAngle - AngleChange;
        ServoWrite(2,CurrentAngle);
      } 
      break;
    case Falling:
      if(ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == PassageOfTime_TIMER){        
        ES_Timer_InitTimer(PassageOfTime_TIMER,IntervalBetweenFalls);
        CurrentAngle = CurrentAngle - AngleChange;
        ServoWrite(2,CurrentAngle);
      }
      else if(ThisEvent.EventType == GAME_OVER){
        ES_Timer_StopTimer(PassageOfTime_TIMER);
        CurrentState = Celebration_POT;
      }  
      else if(ThisEvent.EventType == RESET){
        ServoWrite(2,IcebergUpAngle);        
        ES_Timer_InitTimer(PassageOfTime_TIMER,TimeToRise);
        CurrentState = Raising; 
      }
      break;
    case Celebration_POT:
      if(ThisEvent.EventType == RESET){
        ServoWrite(2,IcebergUpAngle);        
        ES_Timer_InitTimer(PassageOfTime_TIMER,TimeToRise);
        CurrentState = Raising; 
      }
      break;
  }
  return ReturnEvent;
}











