#include "FlipPolarBear.h"
#include "Servo.h"

#define BearUpAngle 0
#define BearDownAngle 120

void FlipDownBear()
{
	ServoWrite(3,BearDownAngle);
}

void FlipUpBear()
{
  ServoWrite(3,BearUpAngle);
}