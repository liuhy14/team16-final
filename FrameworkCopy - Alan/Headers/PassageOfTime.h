#ifndef PASSAGE_OF_TIME_H
#define PASSAGE_OF_TIME_H

#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */
#include "ES_Events.h"   

//State type
typedef enum PotState{ 
  Raising, 
  Welcome_POT, //"Welcome" name clashed with WhackALight.h
  Falling, 
  Celebration_POT, //"Celebration" name clashed with WhackALight.h
  BeforeInit 
} POTState_t ;

//Function Prototypes
bool InitPassageOfTime(uint8_t Priority);
bool PostPOT(ES_Event_t ThisEvent);
ES_Event_t RunPOT(ES_Event_t ThisEvent );

#endif //PASSAGE_OF_TIME_H