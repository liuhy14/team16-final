/****************************************************************************

  Header file for Rainwater.c
  based on the Gen 2 Events and Services Framework
      Author: Varun Nayak
 

 ****************************************************************************/

#ifndef RAINWATER_H
#define RAINWATER_H

#include <stdint.h>
#include <stdbool.h>

#include "ES_Events.h"

typedef enum { RainwaterPseudoState, WelcomeRainwater, WaitingRainwater, Raining, CelebrationRainwater } RainwaterState_t ;



// Public Function Prototypes

bool InitRainwater(uint8_t Priority);
bool PostRainwater(ES_Event_t ThisEvent);
ES_Event_t RunRainwater(ES_Event_t ThisEvent);
bool MapServo(void);	//event checker

#endif 

