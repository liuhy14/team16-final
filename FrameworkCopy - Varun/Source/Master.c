/****************************************************************************
 Module
   Master.c

 Revision
   1.0.1

 Description
   This is a template file for implementing flat state machines under the
   Gen2 Events and Services Framework.
       Author: Varun Nayak


 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/15/12 11:12 jec      revisions for Gen2 framework
 11/07/11 11:26 jec      made the queue static
 10/30/11 17:59 jec      fixed references to CurrentEvent in RunTemplateSM()
 10/23/11 18:20 jec      began conversion from SMTemplate.c (02/20/07 rev)
 
 TODO:
 
 Add:
 header file containing stamp function
header file containing flip polar bear function
header file that gives access to state of leaf inserted/not inserted
 
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"



//header file containing function that gives access to writing messages to LCD (single line)

#include "Master.h"
#include "Servo.h"
#include "AnalogDigital.h"
#include "PassageOfTime.h"
#include "LEAF.h"
#include "FlipPolarBear.h"


/*----------------------------- Module Defines ----------------------------*/

#define GAME_TIMER_NUMBER 0
#define INACTIVITY_TIMER_NUMBER 1
#define CELEB_TIMER_NUMBER 2

#define ONE_SEC 976

#define GAME_TIMER_TIME (60*ONE_SEC) //60 seconds
#define INACTIVITY_TIMER_TIME (30*ONE_SEC) //30 seconds
#define CELEB_TIMER_TIME (10*ONE_SEC)	//10 seconds

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this machine.They should be functions
   relevant to the behavior of this state machine
*/

/*---------------------------- Module Variables ---------------------------*/
// everybody needs a state variable, you may need others as well.
// type of state variable should match htat of enum in header file
static MasterState_t CurrentState;

// with the introduction of Gen2, we need a module level Priority var as well
static uint8_t MyPriority;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitMaster

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, sets up the initial transition and does any
     other required initialization for this state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 18:55
****************************************************************************/
bool InitMaster(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
	
	//set timers
	ES_Timer_SetTimer(GAME_TIMER_NUMBER,GAME_TIMER_TIME);
	ES_Timer_SetTimer(INACTIVITY_TIMER_NUMBER,INACTIVITY_TIMER_TIME);
	ES_Timer_SetTimer(CELEB_TIMER_NUMBER,CELEB_TIMER_TIME);
	
	//Initialize Servo and A/D pins

	
  // put us into the Initial PseudoState
  CurrentState = MasterPseudoState;
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostMaster

 Parameters
     EF_Event ThisEvent , the event to post to the queue

 Returns
     boolean False if the Enqueue operation failed, True otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostMaster(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunMaster

 Parameters
   ES_Event : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes
   uses nested switch/case to implement the machine.
 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunMaster(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
	MasterState_t NextState = CurrentState;

  switch (CurrentState)
  {
				
		
    case MasterPseudoState:        // If current state is initial Psedudo State
    {
      if (ThisEvent.EventType == ES_INIT)    // only respond to ES_Init
      {
				NextState = WelcomeMaster;
				FlipUpBear();//bear diasappear
        //puts("Next State: Welcome \n \r");

      }
    }
    break;

		
		
		
		
    case WelcomeMaster:        // If current state is state one
    {
			if(  (ThisEvent.EventType == START_BUTTON_PRESSED)  &&  ( ReadLeafState() )  )	//CHECK START BUTTON EVENT NAME AND LeafInserted func name
			{
				ES_Timer_SetTimer(GAME_TIMER_NUMBER,GAME_TIMER_TIME); 
				ES_Timer_StartTimer(GAME_TIMER_NUMBER); // Starts the Game Timer
        
        // commented out to disable inactivity timer
				//ES_Timer_SetTimer(INACTIVITY_TIMER_NUMBER,INACTIVITY_TIMER_NUMBER); 
				//ES_Timer_StartTimer(INACTIVITY_TIMER_NUMBER); // Starts the Inactivity Timer
        
				ES_Event_t PostEvent;
				PostEvent.EventType = GAME_START;
				ES_PostAll(PostEvent);
				NextState = PlayingMaster;
        //puts("Next State: Playing \n \r");
								
			}
		}
		break;
		
		
		
		
		
		case PlayingMaster:        // If current state is state one
    {
			if((ThisEvent.EventType == ES_TIMEOUT)&&(ThisEvent.EventParam == INACTIVITY_TIMER_NUMBER))
			{
				ES_Event_t PostEvent;
				PostEvent.EventType = RESET;
				ES_PostAll(PostEvent);
				FlipDownBear();//bear appear
				ES_Timer_StopTimer(GAME_TIMER_NUMBER);
				NextState = WelcomeMaster;
      // puts("INACTIVITY Reset \n \r");

			}
			if(ThisEvent.EventType == LEAF_REMOVED) 	//LEAF REMOVED NAME CHECK
			{
				ES_Event_t PostEvent;
				PostEvent.EventType = RESET;
				ES_PostAll(PostEvent);
				FlipUpBear();//bear diasappear
				ES_Timer_StopTimer(GAME_TIMER_NUMBER);
				ES_Timer_StopTimer(INACTIVITY_TIMER_NUMBER);
				NextState = WelcomeMaster;
       //puts("Leaf Removed, Go to Welcome State \n \r");
			}
			if((ThisEvent.EventType == ES_TIMEOUT)&&(ThisEvent.EventParam == GAME_TIMER_NUMBER))
			{
				ES_Event_t PostEvent;
				PostEvent.EventType = GAME_OVER;
				ES_PostAll(PostEvent);
				ES_Timer_StopTimer(INACTIVITY_TIMER_NUMBER);
				FlipDownBear();//bear appear
				stamp();  //stamps
				ES_Timer_SetTimer(CELEB_TIMER_NUMBER,CELEB_TIMER_TIME);
				ES_Timer_StartTimer(CELEB_TIMER_NUMBER);
				NextState = CelebrationMaster;
        //puts("Game ended (timer), Go to Celebration \n \r");				
			}				
		}
		break;
		
		
		
		
		
		case CelebrationMaster:        // If current state is state one
    {
			if((ThisEvent.EventType == ES_TIMEOUT)&&(ThisEvent.EventParam == CELEB_TIMER_NUMBER))
			{
				ES_Event_t PostEvent;
				PostEvent.EventType = RESET;
				ES_PostAll(PostEvent);
				FlipUpBear();//bear diasappear
				NextState = WelcomeMaster;
       // puts("Celebration over, go to Welcome State\n \r");
			}
		}
		break;		
		
		
		
		
	}// end switch on Current State
	
	CurrentState = NextState;
  return ReturnEvent;
}

/****************************************************************************
 Function
     QueryTemplateSM

 Parameters
     None

 Returns
     MasterState_t The current state of the Master state machine

 Description
     returns the current state of the Master state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:21
****************************************************************************/
MasterState_t QueryMaster(void)
{
  return CurrentState;
}

/***************************************************************************
 private functions
 ***************************************************************************/

