/****************************************************************************
Servo.c
    Module for interfacing AD Pins
		
		TODO:
		Interface additional AD pins ifreq
****************************************************************************/
//#define TEST


#include <stdint.h>
#include <stdbool.h>

// the headers to access the GPIO subsystem
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

// the headers to access the TivaWare Library
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"

#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_Port.h"
#include "termio.h"
#include "EnablePA25_PB23_PD7_PF0.h"
#include "BITDEFS.H"

#define clrScrn() printf("\x1b[2J")
#define goHome() printf("\x1b[1,1H")
#define clrLine() printf("\x1b[K")

// the header for definition of constants


#include "AnalogDigital.h" // the header file for this module

#include "ADMulti.h"

//readability defines

#define POT_PIN BIT0LO	//PE0
#define LEAF_PIN BIT1LO	//PE1

#define NO_OF_AD_PINS 2

void ADInit(void)
{
    // enable the system clock into Port E
    HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R4;

    // wait until the peripheral reports its clock as ready
    while ((HWREG(SYSCTL_PRGPIO) & SYSCTL_PRGPIO_R4) != SYSCTL_PRGPIO_R4)
    {
        // don't do anyting till the peripheral clock is ready
    }

    // program servo pins as input
    HWREG(GPIO_PORTE_BASE+GPIO_O_DIR) &= ( POT_PIN & LEAF_PIN );

    // intitialize two channels PE0 an PE1 to analog input as per AD library
    ADC_MultiInit(NO_OF_AD_PINS);
    

}


/*************************
      TEST HARNESS
*************************/

/* TEST HARNESS */

#ifdef TEST
/*testharness*/

int main(void)
{
  puts("here");
  SysCtlClockSet(SYSCTL_SYSDIV_5 | SYSCTL_USE_PLL | SYSCTL_OSC_MAIN | SYSCTL_XTAL_16MHZ);
  TERMIO_Init();
  
  printf("Inside main \n\r");
  
  //uint8_t CurrentInputState = 0;
  ES_Initialize(ES_Timer_RATE_1mS);
  
  ADInit();
	
	while(1)
	{
		uint32_t analog_read[NO_OF_AD_PINS];
		ADC_MultiRead(analog_read);
		printf("Analog Value = %d \n \r",analog_read[0]);
	}

     
  
  
  return 0;
}


#endif
