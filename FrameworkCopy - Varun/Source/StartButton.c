/*----------------------------- Include Files -----------------------------*/
//include header files for this service
#include "StartButton.h"

//Header for Timing Functions
#include "ES_Port.h"

//include header files for the framework
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"
#include "ES_ShortTimer.h"

//Include Port B functions
#include "Ports.h"

//include header files for the other modules that are referenced
#include "Master.h"

/*----------------------------- Module Defines ----------------------------*/
#define DEBOUNCE_TIME 20 //ms
#define DEBOUNCE_TIMER 10

//Start button is PD0
#define START_BUTTON_PIN GPIO_PIN_0
#define START_BUTTON_LO  BIT0LO
#define START_BUTTON_HI  BIT0HI

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service. */



/*---------------------------- Module Variables ---------------------------*/
//with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;
static StartButtonState_t LastButtonState; //For the state machine
static uint8_t LastPinState; //Logic level on the pin
/*------------------------------ Module Code ------------------------------*/
/****************************************************************************/


/****************************************************************************
Function:       InitButtonDebounce
Parameters:     uint8_t : the priorty of this service
Returns:        bool, false if error in initialization, true otherwise
Description:    Saves away the priority, and does any other required initialization
                for this service
****************************************************************************/
bool InitButtonDebounce(uint8_t Priority)
{
  ES_Event_t ThisEvent;
  MyPriority = Priority;

  //Initialize the port line to monitor the button
  SetPin2Digital_PortD(START_BUTTON_PIN);
  SetPin2Input_PortD(START_BUTTON_PIN);
  //Sample the button port pin and use it to initialize LastPinState
  LastPinState = (PORTD & START_BUTTON_PIN);
  //Set CurrentState to be DEBOUNCING
  LastButtonState = Debouncing;
  //Start debounce timer (timer posts to ButtonDebounceSM)
  ES_Timer_InitTimer(DEBOUNCE_TIMER, DEBOUNCE_TIME);
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService( MyPriority, ThisEvent) == true)
  {
    return true;
  }else
  {
    return false;
  }
}

/****************************************************************************
Function:     PostButtonDebounce
Parameters:   EF_Event ThisEvent ,the event to post to the queue
Returns:      bool false if the Enqueue operation failed, true otherwise
Description:  Posts an event to this state machine's queue
****************************************************************************/
bool PostButtonDebounce(ES_Event_t ThisEvent)
{
  return ES_PostToService( MyPriority, ThisEvent);
}

/****************************************************************************
Function:     RunButtonDebounce
Parameters:   ES_Event_t : the event to process
Returns:      ES_Event_t, ES_NO_EVENT if no error ES_ERROR otherwise
Description:  add your description here
****************************************************************************/
ES_Event_t RunButtonDebounce(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ES_Event_t Event2Post;
  ReturnEvent.EventType = ES_NO_EVENT;    // assume no errors
  StartButtonState_t NextButtonState = LastButtonState; //Initialize NextState to CurrentState

  switch (LastButtonState)
  {
    case Debouncing :
      //If EventType is ES_TIMEOUT & parameter is debounce timer number
      if( ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == DEBOUNCE_TIMER)
      {
        NextButtonState = Ready2Sample;
      }
    break;
    case Ready2Sample :
      if( ThisEvent.EventType == START_BUTTON_UP )
      {
        //Start debounce timer
        ES_Timer_InitTimer(DEBOUNCE_TIMER, DEBOUNCE_TIME);
        //Set the next state
        NextButtonState = Debouncing;
        //Don't need to post a button up event to anything, so do nothing.
      }
      if( ThisEvent.EventType == START_BUTTON_DOWN)
      {
        //Start debounce timer
        ES_Timer_InitTimer(DEBOUNCE_TIMER, DEBOUNCE_TIME);
        //Set next state
        NextButtonState = Debouncing;
        //Post Debounced start button event up to master SM_
        Event2Post.EventType = START_BUTTON_PRESSED;
        PostMaster(Event2Post);
      }
    break;
  }
  LastButtonState = NextButtonState;
  return ReturnEvent;
}


/****************************************************************************
Function: 	  CheckButtonEvents
Parameters: 	None
Returns: 	    bool: true if a new event was detected
Description:	description here
Notes:
****************************************************************************/
bool CheckStartButton(void)
{
  uint8_t   CurrentPinState;
  bool      ReturnVal = false;

  //Set CurrentPinState to state read from port pin
  CurrentPinState = (PORTD & START_BUTTON_PIN);

  //If the CurrentPinState is different from the LastButtonState
  if (CurrentPinState != LastPinState)
  {
    ES_Event_t ThisEvent; //An event has occured
    //If the CurrentPinState is LOW (Button down)
    if(CurrentPinState == 0)
    {
      //PostEvent ButtonDown to ButtonDebounce queue
      ThisEvent.EventType = START_BUTTON_DOWN;
      PostButtonDebounce(ThisEvent);
    }
    else
    {
      //PostEvent ButtonUp to ButtonDebounce queue
      ThisEvent.EventType = START_BUTTON_UP;
      PostButtonDebounce(ThisEvent);
    }
    ReturnVal = true;
  }
  LastPinState = CurrentPinState; // update the state for next time

  return ReturnVal;
}
