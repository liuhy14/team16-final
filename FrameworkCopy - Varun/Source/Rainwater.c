/****************************************************************************
 Module
   Rainwater.c

 Revision
   1.0.1

 Description
   This is a Rainwater file for implementing flat state machines under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/15/12 11:12 jec      revisions for Gen2 framework
 11/07/11 11:26 jec      made the queue static
 10/30/11 17:59 jec      fixed references to CurrentEvent in RunTemplateSM()
 10/23/11 18:20 jec      began conversion from SMTemplate.c (02/20/07 rev)
 
 
 TODO:
 What                                         Who
 PWMTivaInit() goes in Master                 Varun (resolved - put it in main with other port enabling code)
  Check angle allowances for matching         Varun
  POT To Linear motion is nonlinear, find 
  an expression to linearize                  Varun
****************************************************************************/

//#define TEST


/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include <stdint.h>
#include <stdbool.h>

// the headers to access the GPIO subsystem
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

// the headers to access the TivaWare Library
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"

#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_Port.h"
#include "termio.h"
#include "EnablePA25_PB23_PD7_PF0.h"
#include "BITDEFS.H"

#define clrScrn() printf("\x1b[2J")
#define goHome() printf("\x1b[1,1H")
#define clrLine() printf("\x1b[K")


#include "PWM16Tiva.h"
#include "ADMulti.h"

#include "Servo.h"	//to servo
#include "AnalogDigital.h" //to initialize all A/D pins
#include "Rainwater.h"
#include "Points.h"	//to post points



/*----------------------------- Module Defines ----------------------------*/


#define RAIN_TIMER_NUMBER 3
#define RAIN_PULSE_TIMER_NUMBER 12

#define ONE_SEC 976
#define RAIN_TIMER_TIME (4*ONE_SEC) 
#define RAIN_PULSE_TIMER_TIME (0.25*ONE_SEC)


#define BUCKET_SERVO_CHANNEL 2
#define HOME_DUTY_CYCLE 50


#define POT_INPUT_CHANNEL 0 //PE0 for zeroeth element of array

#define NUMBER_OF_ANALOG_INPUTS 4	//change this for integration

#define LEDCOL1PIN BIT1HI	//PF1
#define LEDCOL2PIN BIT2HI	//PF2
#define LEDCOL3PIN BIT3HI	//PF3

#define RAIN_POINTSVAL 2	//points value



/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this machine.They should be functions
   relevant to the behavior of this state machine
*/

static void LCDHWInit(void);
bool MapServo(void);
static void LEDControl(uint8_t WhichLED,uint8_t State);


/*---------------------------- Module Variables ---------------------------*/
// everybody needs a state variable, you may need others as well.
// type of state variable should match htat of enum in header file
static RainwaterState_t CurrentState;

// with the introduction of Gen2, we need a module level Priority var as well
static uint8_t MyPriority;

static uint8_t LitColumn;
static uint8_t PotPosition;
static uint8_t BlinkState = 0; //for toggling the pulsing of the LED during rain

static uint32_t Analog_Read[NUMBER_OF_ANALOG_INPUTS];



/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitRainwater

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, sets up the initial transition and does any
     other required initialization for this state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 18:55
****************************************************************************/
bool InitRainwater(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
	
	ES_Timer_SetTimer(RAIN_TIMER_NUMBER,RAIN_TIMER_TIME); 	//Call Timer Inits for RainTimer

	/*
	ServoInit and ADInit are called in test harness main()
	In integration, it will be called in main()
	*/
	
	//Initialize PWM pin of servo and Servo.write to home positon
	//ServoWrite(1,0); //servo1 (bucket) set to angle 0 degrees			

	//Initialize led pins 1,2,3 (for each column)
	LCDHWInit(); //initializes LCD hardware and initializes control to low (lights off)
	
	CurrentState = RainwaterPseudoState; //Set CurrentState to RainwaterPseudoState

  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostRainwater

 Parameters
     EF_Event ThisEvent , the event to post to the queue

 Returns
     boolean False if the Enqueue operation failed, True otherwise

 Description
     Posts an event to this state machine's queue
 Notes


****************************************************************************/
bool PostRainwater(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunRainwater

 Parameters
   ES_Event : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes
   uses nested switch/case to implement the machine.

****************************************************************************/
ES_Event_t RunRainwater(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
	RainwaterState_t NextState;
	NextState = CurrentState;

  switch (CurrentState)
  {
    case RainwaterPseudoState:        // If current state is initial Psedudo State
    {
      if (ThisEvent.EventType == ES_INIT)    // only respond to ES_Init
      {	
				//turn on all LEDs
				LEDControl(1,1);	//turn left LED on
				LEDControl(2,1);	//turn mid LED on
				LEDControl(3,1);	//turn right LED on
				
        NextState = WelcomeRainwater;
        //puts("PseudoState \n \r");
      }
    }
    break;

		
		
    case WelcomeRainwater:        // If current state is WelcomeRainwater
    {
             

        
      switch (ThisEvent.EventType)
      {
        case GAME_START:  //If event is 
        {  
					//turn off all LEDs
					LEDControl(1,0);
					LEDControl(2,0);
					LEDControl(3,0);
					
					ES_Timer_SetTimer(RAIN_TIMER_NUMBER,RAIN_TIMER_TIME); 	//set to 5 seconds
					ES_Timer_StartTimer(RAIN_TIMER_NUMBER); // Starts the Timer
					
          NextState = WaitingRainwater;  //Decide what the next state will be
          //puts("Starting game.. \n \r");
        }
        break;        
      }  
		}
    break;
		
		
		
		case WaitingRainwater:        // If current state is WelcomeRainwater
    {
      switch (ThisEvent.EventType)
      {
        case ES_TIMEOUT:  //If event is 
        {   
          if(ThisEvent.EventParam == RAIN_TIMER_NUMBER)
					{
						ES_Timer_SetTimer(RAIN_TIMER_NUMBER,RAIN_TIMER_TIME); 	//Call Timer Inits for RainTimer
						ES_Timer_StartTimer(RAIN_TIMER_NUMBER); // Starts the Timer
						LitColumn = (rand()%3)+1; 	//random number 1,2 or 3
            //printf("Random LED at the moment = %d \n \r", LitColumn);
						LEDControl(LitColumn,1);	//turn on
						BlinkState = 1;//to start with 1 in order to toggle
						NextState = Raining;
						ES_Timer_SetTimer(RAIN_PULSE_TIMER_NUMBER,RAIN_PULSE_TIMER_TIME);
						ES_Timer_StartTimer(RAIN_PULSE_TIMER_NUMBER); //start pulse timer here
            //puts("Start Raining Timeout, timer started again \n \r");
					}					
        }
        break;
				
				case RESET:
				{
					ES_Timer_StopTimer(RAIN_TIMER_NUMBER);	//clear rain timer
					//turn oon all LEDs
					LEDControl(1,1);
					LEDControl(2,1);
					LEDControl(3,1);
					
					NextState = WelcomeRainwater;
          //puts("Waiting and RESET \n \r");

					
				}				
				break;
					
        
      }  
		}
    break;
		
		
		
		case Raining:        // If current state is WelcomeRainwater
    {
      switch (ThisEvent.EventType)
      {
        case ES_TIMEOUT:  //If event is 
        {   
          if(ThisEvent.EventParam == RAIN_TIMER_NUMBER)
					{
						ES_Timer_SetTimer(RAIN_TIMER_NUMBER,RAIN_TIMER_TIME); 	//Call Timer Inits for RainTimer
						ES_Timer_StartTimer(RAIN_TIMER_NUMBER); // Starts the Timer
						ES_Timer_StopTimer(RAIN_PULSE_TIMER_NUMBER); //stops the pulsing timer
						if(PotPosition == LitColumn)
						{
							ES_Event_t PostEvent;
							PostEvent.EventType = POST_POINTS;
							PostEvent.EventParam = RAIN_POINTSVAL;
							PostPoints(PostEvent);
              //puts("**********SCORE********** \n \r");
						}
						//turn off all LEDs (to be on the safe side)
            LEDControl(1,0);
            LEDControl(2,0);
            LEDControl(3,0);
						NextState = WaitingRainwater;
           //puts("Raining and Rain Timeout all LEDs off, timer started again \n \r");

					}
		 if(ThisEvent.EventParam == RAIN_PULSE_TIMER_NUMBER)
		 {	 
			 BlinkState = (BlinkState == 0 );	//toggles BlinkState
			 LEDControl(LitColumn,BlinkState);	//blinks the LitColumn
			 ES_Timer_SetTimer(RAIN_PULSE_TIMER_NUMBER,RAIN_PULSE_TIMER_TIME);	//restarts the timer 
			ES_Timer_StartTimer(RAIN_PULSE_TIMER_NUMBER);
		 }
        }
				break;
				
				case RESET:
				{
					ES_Timer_StopTimer(RAIN_TIMER_NUMBER);	//clear rain timer
					//turn on all LEDs
					LEDControl(1,1);
					LEDControl(2,1);
					LEDControl(3,1);
					
					NextState = WelcomeRainwater;
          //puts("Raining and RESET \n \r");

					
				}				
				break;

				case GAME_OVER:
				{
					//turn on all LEDs
					LEDControl(1,1);	//turn left LED on
					LEDControl(2,1);	//turn mid LED on
					LEDControl(3,1);	//turn right LED on
					ES_Timer_StopTimer(RAIN_TIMER_NUMBER);	//clear rain timer
					NextState = CelebrationRainwater;
					
				}
				break;
				
			}//end event switch 
		}
			break; //end raining
		
		
		case CelebrationRainwater:        // If current state is WelcomeRainwater
    {
      switch (ThisEvent.EventType)
      {
        case RESET:
				{
					ES_Timer_StopTimer(RAIN_TIMER_NUMBER);	//clear rain timer
					//turn on all LEDs
					LEDControl(1,1);
					LEDControl(2,1);
					LEDControl(3,1);
					
					NextState = WelcomeRainwater;
          //puts("Celebration \n \r");
					
				}
				break;
      }  
		}
    break;
		
		
    
  }       // end switch on Current State
  
	CurrentState = NextState;
	
	return ReturnEvent;
}

/****************************************************************************
 Function
     QueryRainwater

 Parameters
     None

 Returns
     RainwaterState_t The current state of the Template state machine

 Description
     returns the current state of the Template state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:21
****************************************************************************/
RainwaterState_t QueryRainwater(void)
{
  return CurrentState;
}



/***************************************************************************
Event Checker
/**************************************************************************/

//Maps analog pot input to servo angle and writes to the servo
//Always returns false as we don't want to register event but we always want to run it
bool MapServo()
{
	uint16_t	Angle;
	
	ADC_MultiRead(Analog_Read);
	
	Angle = (Analog_Read[0]*180)/4095; 	//linear mapping, 0 of Analog_Read corresponds to PE0 Pot
  
  //printf("Angle = %d \n \r",Angle);
	ServoWrite(1,Angle);	//1 corresponds to first servo which is channel 0 which is bucket servo
	
	if (Angle<=20)  //0-20 allowance for left rainwater column
	{
		PotPosition =1;
    
	}
	else if ((Angle<=110)&&(Angle>=70)) //70-110 allowance for middle rainwater column
	{
		PotPosition = 2;
    

	}
	else if ((Angle<=200)&&(Angle>=150)) //150-200 allowance for right rainwater column
	{
		PotPosition = 3;
       

	}
	else
	{
	}
	
	return 0;
}




/***************************************************************************
 private functions
 ***************************************************************************/



static void LCDHWInit(void)
{
	
  //Enabling of PORTF is taken care of by main() in integration - Spencer  
	 HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R5;	//enable Port F
    while( (HWREG(SYSCTL_PRGPIO)&SYSCTL_PRGPIO_R5) != SYSCTL_PRGPIO_R5)
      {
      }
  
     
    HWREG(GPIO_PORTF_BASE+GPIO_O_DEN) |=  (LEDCOL1PIN | LEDCOL2PIN | LEDCOL3PIN) ;	//port F bits 1,2,3 is assigned as digital PF1,2,3 for LED columns
    HWREG(GPIO_PORTF_BASE+GPIO_O_DIR) |=  (LEDCOL1PIN | LEDCOL2PIN | LEDCOL3PIN);  //port F bits 1,2,3 assigned as output by setting the bit
     
      
  // start with all lights off
    HWREG(GPIO_PORTF_BASE+(GPIO_O_DATA + ALL_BITS)) &= (~LEDCOL1PIN);
	  HWREG(GPIO_PORTF_BASE+(GPIO_O_DATA + ALL_BITS)) &= (~LEDCOL2PIN);
    HWREG(GPIO_PORTF_BASE+(GPIO_O_DATA + ALL_BITS)) &= (~LEDCOL3PIN);   
    
	
}

//Which LED is 1,2,3 for left, mid or right col
//State is either 1 or 0 for on or off
static void LEDControl(uint8_t WhichLED,uint8_t State)
{
	if(WhichLED == 1)//1 is left column
	{
		if(State)	//if command is to turn ON
		{
			HWREG(GPIO_PORTF_BASE+(GPIO_O_DATA + ALL_BITS)) |= LEDCOL1PIN; //turn on right LED 
            //puts("lighting LED 1 \n \r");

		}
		else
		{
			HWREG(GPIO_PORTF_BASE+(GPIO_O_DATA + ALL_BITS)) &= (~LEDCOL1PIN); //turn off right LED
		}
  }
	
	else if(WhichLED == 2)//2 is mid column
	{
		if(State)	//if command is to turn ON
		{
			HWREG(GPIO_PORTF_BASE+(GPIO_O_DATA + ALL_BITS)) |= LEDCOL2PIN; //turn on right LED 
            //puts("lighting LED 2 \n \r");

		}
		else
		{
			HWREG(GPIO_PORTF_BASE+(GPIO_O_DATA + ALL_BITS)) &= (~LEDCOL2PIN); //turn off right LED
		}
  }
	
	else if(WhichLED == 3)//3 is right column
	{
		if(State)	//if command is to turn ON
		{
			HWREG(GPIO_PORTF_BASE+(GPIO_O_DATA + ALL_BITS)) |= LEDCOL3PIN; //turn on right LED 
            //puts("lighting LED 3 \n \r");

		}
		else
		{
			HWREG(GPIO_PORTF_BASE+(GPIO_O_DATA + ALL_BITS)) &= (~LEDCOL3PIN); //turn off right LED
		}
	}
  
  else;
}



/*************************
      TEST HARNESS
*************************/


/* TEST HARNESS */

#ifdef TEST
/*testharness*/

int main(void)
{ 
  TERMIO_Init();
   puts("here");
// Set the clock to run at 40MhZ using the PLL and 16MHz external crystal
  SysCtlClockSet(SYSCTL_SYSDIV_5 | SYSCTL_USE_PLL | SYSCTL_OSC_MAIN
      | SYSCTL_XTAL_16MHZ);  
  
  printf("Inside main \n\r");
  
  
  //uint8_t CurrentInputState = 0;
  ES_Initialize(ES_Timer_RATE_1mS);
  
  puts("here");
  
  ServoInit();
	ADInit();
  LCDHWInit();
	      
	
    LEDControl(1,1);
    LEDControl(2,0);
    LEDControl(3,1);

  
  return 0;
}

#endif
