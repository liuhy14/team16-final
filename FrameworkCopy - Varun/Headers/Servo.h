/****************************************************************************
Servo.h
    Module for interfacing Servos
    Author: Varun Nayak
****************************************************************************/

#ifndef SERVO_H
#define SERVO_H

#include <stdint.h>
#include <stdbool.h>

void ServoInit(void);
void ServoWrite( uint8_t ServoNum, uint16_t Angle);


#endif