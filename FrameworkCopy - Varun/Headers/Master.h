/****************************************************************************

  Header file for Master.c
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef MASTER_H
#define MASTER_H

#include <stdint.h>
#include <stdbool.h>

#include "ES_Events.h"

typedef enum { 
  MasterPseudoState, 
  WelcomeMaster,  
  PlayingMaster ,  
  CelebrationMaster 
} MasterState_t ;

// Public Function Prototypes

bool InitMaster(uint8_t Priority);
bool PostMaster(ES_Event_t ThisEvent);
ES_Event_t RunMaster(ES_Event_t ThisEvent);

#endif 

