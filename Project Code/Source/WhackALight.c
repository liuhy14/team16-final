/****************************************************************************
 Module
   WHackALight.c

 Revision
   1.0.1

 Description
   This is the first service for the MorseElements under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 10/26/17 18:26 jec     moves definition of ALL_BITS to ES_Port.h
 10/19/17 21:28 jec     meaningless change to test updating
 10/19/17 18:42 jec     removed referennces to driverlib and programmed the
                        ports directly
 08/21/17 21:44 jec     modified LED blink routine to only modify bit 3 so that
                        I can test the new new framework debugging lines on PF1-2
 08/16/17 14:13 jec      corrected ONE_SEC constant to match Tiva tick rate
 11/02/13 17:21 jec      added exercise of the event deferral/recall module
 08/05/13 20:33 jec      converted to test harness service
 01/16/12 09:58 jec      began conversion from TemplateFSM.c
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
#define TEST

// This module
#include "WhackALight.h"

// Hardware
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

// Event & Services Framework
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"
#include "ES_ShortTimer.h"

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

#include "driverlib/sysctl.h"

#include "ES_Port.h"
#include "termio.h"
#include "EnablePA25_PB23_PD7_PF0.h"

//Header for EnableAllPorts function
#include "Ports.h"

#define clrScrn() printf("\x1b[2J")
#define goHome() printf("\x1b[1,1H")
#define clrLine() printf("\x1b[K")

/*----------------------------- Module Defines ----------------------------*/
// these times assume a 1.000mS/tick timing
#define ONE_SEC 1000
#define HALF_SEC (ONE_SEC / 2)
#define TWO_SEC (ONE_SEC * 2)
#define FIVE_SEC (ONE_SEC * 5)
#define SENSOR_1 GPIO_PIN_4
#define SENSOR_2 GPIO_PIN_5
#define SENSOR_3 GPIO_PIN_6
#define SENSOR_4 GPIO_PIN_7
#define LED_1 GPIO_PIN_2
#define LED_2 GPIO_PIN_3
#define LED_3 GPIO_PIN_6
#define LED_4 GPIO_PIN_7

#define SENSOR_1_HI BIT4HI
#define SENSOR_2_HI BIT5HI
#define SENSOR_3_HI BIT6HI
#define SENSOR_4_HI BIT7HI
#define LED_1_HI BIT2HI
#define LED_2_HI BIT3HI
#define LED_3_HI BIT6HI
#define LED_4_HI BIT7HI

#define SENSOR_1_LO BIT4LO
#define SENSOR_2_LO BIT5LO
#define SENSOR_3_LO BIT6LO
#define SENSOR_4_LO BIT7LO
#define LED_1_LO BIT2LO
#define LED_2_LO BIT3LO
#define LED_3_LO BIT6LO
#define LED_4_LO BIT7LO

#define TIMER1 300
#define TIMER2 500
#define TIMER3 600
#define TIMER4 1000

// #define ALL_BITS (0xff<<2)   Moved to ES_Port.h
/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/
static void FlashLEDs(void);

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;
static WhackALightState_t CurrentState;
static uint8_t LastInputState;
static uint8_t CurrentInputState;
// add a deferral queue for up to 3 pending deferrals +1 to allow for ovehead
static ES_Event_t DeferralQueue[3 + 1];

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitializeWhackALight

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any
     other required initialization for this service
 Notes

 Author
     J. Edward Carryer, 01/16/12, 10:00
****************************************************************************/
bool InitializeWhackALight(uint8_t Priority)
{

  ES_Event_t ThisEvent;

  MyPriority = Priority;
  /********************************************
   in here you write your initialization code
   *******************************************/
  // initialize deferral queue for testing Deferal function
  ES_InitDeferralQueueWith(DeferralQueue, ARRAY_SIZE(DeferralQueue));

  // set up I/O lines for debugging
  // enable the clock to Port C for sensors and Port D for leds
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R2;
  // enable the clock to Port D for leds
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R3;
  // kill a few cycles to let the peripheral clock get going
  while (((HWREG(SYSCTL_PRGPIO) & SYSCTL_RCGCGPIO_R2) != SYSCTL_RCGCGPIO_R2) && 
  	((HWREG(SYSCTL_PRGPIO) & SYSCTL_RCGCGPIO_R3) != SYSCTL_RCGCGPIO_R3))
  {}
  // Enable pins for digital I/O setting port C as inputs and port D as outputs
  HWREG(GPIO_PORTC_BASE+GPIO_O_DEN) |= (SENSOR_1_HI | SENSOR_2_HI | SENSOR_3_HI | SENSOR_4_HI);
  HWREG(GPIO_PORTD_BASE+GPIO_O_DEN) |= (LED_1_HI | LED_2_HI | LED_3_HI | LED_4_HI);
  // Set direction of port D to outputs
  HWREG(GPIO_PORTD_BASE+GPIO_O_DIR) |= (LED_1_HI | LED_2_HI | LED_3_HI | LED_4_HI);

  // Write all four LEDs high
  HWREG(GPIO_PORTD_BASE + (GPIO_O_DATA + ALL_BITS)) &= (LED_1_HI & LED_2_HI & LED_3_HI & LED_4_HI);

  LastInputState = HWREG(GPIO_PORTC_BASE + (GPIO_O_DATA + ALL_BITS));
  CurrentState = Welcome;

  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostWhackALight

 Parameters
     EF_Event ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostWhackALight(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunWhackALightSM

 Parameters
   ES_Event : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes

 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunWhackALightSM(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
  WhackALightState_t NextState;
	
  NextState = CurrentState;

  switch (CurrentState)
  {
    case Welcome:
    {
    	HWREG(GPIO_PORTD_BASE + (GPIO_O_DATA + ALL_BITS)) &= (LED_1_HI & LED_2_HI & LED_3_HI & LED_4_HI);
      if (ThisEvent.EventType == ES_INIT){
				printf("\rWelcome to the welcome state.\r\n");
		}else if (ThisEvent.EventType == GAME_START){
			//Enter playing mode
			NextState = WhackMe;
		}
    }
    break;
    case WhackMe:   //Whacking mode, if any led is covered, turn off LED, and start timer
    {
			if(ThisEvent.EventType == LED_1_COVERED){
					HWREG(GPIO_PORTD_BASE + (GPIO_O_DATA + ALL_BITS)) &= (LED_1_LO);
					ES_Timer_InitTimer(LED_1_TIMER, TIMER1);
				}else if(ThisEvent.EventType == LED_2_COVERED){
					HWREG(GPIO_PORTD_BASE + (GPIO_O_DATA + ALL_BITS)) &= (LED_2_LO);
					ES_Timer_InitTimer(LED_2_TIMER, TIMER2);
				}else if(ThisEvent.EventType == LED_3_COVERED){
					HWREG(GPIO_PORTD_BASE + (GPIO_O_DATA + ALL_BITS)) &= (LED_3_LO);
					ES_Timer_InitTimer(LED_3_TIMER, TIMER3);
				}else if(ThisEvent.EventType == LED_4_COVERED){
					HWREG(GPIO_PORTD_BASE + (GPIO_O_DATA + ALL_BITS)) &= (LED_4_LO);
					ES_Timer_InitTimer(LED_4_TIMER, TIMER4);
				}else if((ThisEvent.EventType == ES_TIMEOUT) && (ThisEvent.EventParam == LED1_TIMER)){
					HWREG(GPIO_PORTD_BASE + (GPIO_O_DATA + ALL_BITS)) &= (LED_1_HI);
				}else if((ThisEvent.EventType == ES_TIMEOUT) && (ThisEvent.EventParam == LED2_TIMER)){
					HWREG(GPIO_PORTD_BASE + (GPIO_O_DATA + ALL_BITS)) &= (LED_2_HI);
				}else if((ThisEvent.EventType == ES_TIMEOUT) && (ThisEvent.EventParam == LED3_TIMER)){
					HWREG(GPIO_PORTD_BASE + (GPIO_O_DATA + ALL_BITS)) &= (LED_3_HI);
				}else if((ThisEvent.EventType == ES_TIMEOUT) && (ThisEvent.EventParam == LED4_TIMER)){
					HWREG(GPIO_PORTD_BASE + (GPIO_O_DATA + ALL_BITS)) &= (LED_4_HI);
				}else if(ThisEvent.EventType == GAME_OVER){
					NextState = Celebration;
				}else if(ThisEvent.EventType == RESET){
					NextState = Welcome;
				}
			}
		break;
		case Celebration:
		{
			ES_Timer_InitTimer(FLASH_TIMER, 1000);
			if ((ThisEvent.EventType == ES_TIMEOUT) && (ThisEvent.EventParam == FLASH_TIMER)){
				FlashLEDs();
			}else if(ThisEvent.EventType == RESET){
				NextState = Welcome;
			}
		}
		break;
		default: {};
		}
	CurrentState = NextState;

  return ReturnEvent;
}

/***************************************************************************
 private functions
 ***************************************************************************/
static void FlashLEDs(){
	HWREG(GPIO_PORTD_BASE + (GPIO_O_DATA + ALL_BITS)) ^= (LED_1_HI | LED_2_HI | LED_3_HI | LED_4_HI);
}

bool CheckWhackALight_Sensor1(void){
	bool ReturnVal = false;
	CurrentInputState = HWREG(GPIO_PORTC_BASE + (GPIO_O_DATA + ALL_BITS));
	uint8_t LastPinInput = LastInputState & SENSOR_1_HI;
	uint8_t CurrentPinInput =  CurrentInputState & SENSOR_1_HI;
	if ((CurrentPinInput != LastPinInput) && (CurrentPinInput == SENSOR_1_HI)){
		ES_Event_t ThisEvent;
		ThisEvent.EventType = LED_1_COVERED;
		ThisEvent.EventParam = 0;
		PostWhackALight(ThisEvent);
		ReturnVal = true;
	}
	LastInputState = CurrentInputState;
	return ReturnVal;
}

bool CheckWhackALight_Sensor2(void){
	bool ReturnVal = false;
	CurrentInputState = HWREG(GPIO_PORTC_BASE + (GPIO_O_DATA + ALL_BITS));
	uint8_t LastPinInput = LastInputState & SENSOR_2_HI;
	uint8_t CurrentPinInput =  CurrentInputState & SENSOR_2_HI;
	if ((CurrentPinInput != LastPinInput) && (CurrentPinInput == SENSOR_2_HI)){
		ES_Event_t ThisEvent;
		ThisEvent.EventType = LED_2_COVERED;
		ThisEvent.EventParam = 0;
		PostWhackALight(ThisEvent);
		ReturnVal = true;
	}
	LastInputState = CurrentInputState;
	return ReturnVal;
}

bool CheckWhackALight_Sensor3(void){
	bool ReturnVal = false;
	CurrentInputState = HWREG(GPIO_PORTC_BASE + (GPIO_O_DATA + ALL_BITS));
	uint8_t LastPinInput = LastInputState & SENSOR_3_HI;
	uint8_t CurrentPinInput =  CurrentInputState & SENSOR_3_HI;
	if ((CurrentPinInput != LastPinInput) && (CurrentPinInput == SENSOR_3_HI)){
		ES_Event_t ThisEvent;
		ThisEvent.EventType = LED_3_COVERED;
		ThisEvent.EventParam = 0;
		PostWhackALight(ThisEvent);
		ReturnVal = true;
	}
	LastInputState = CurrentInputState;
	return ReturnVal;
}

bool CheckWhackALight_Sensor4(void){
	bool ReturnVal = false;
	CurrentInputState = HWREG(GPIO_PORTC_BASE + (GPIO_O_DATA + ALL_BITS));
	uint8_t LastPinInput = LastInputState & SENSOR_4_HI;
	uint8_t CurrentPinInput =  CurrentInputState & SENSOR_4_HI;
	if ((CurrentPinInput != LastPinInput) && (CurrentPinInput == SENSOR_4_HI)){
		ES_Event_t ThisEvent;
		ThisEvent.EventType = LED_4_COVERED;
		ThisEvent.EventParam = 0;
		PostWhackALight(ThisEvent);
		ReturnVal = true;
	}
	LastInputState = CurrentInputState;
	return ReturnVal;
}

/************************** TEST HARNESS ************************************/
#ifdef TEST 
int main(){  
	ES_Return_t ErrorType;

  // Set the clock to run at 40MhZ using the PLL and 16MHz external crystal
  SysCtlClockSet(SYSCTL_SYSDIV_5 | SYSCTL_USE_PLL | SYSCTL_OSC_MAIN
      | SYSCTL_XTAL_16MHZ);
  TERMIO_Init();
  clrScrn();

  // When doing testing, it is useful to announce just which program
  // is running.
  puts("\rStarting Test Harness for WhackALight \r");
  printf( "the 2nd Generation Events & Services Framework V2.4\r\n");
  printf( "%s %s\n", __TIME__, __DATE__);
  printf( "\n\r\n");

  // reprogram the ports that are set as alternate functions or
  // locked coming out of reset. (PA2-5, PB2-3, PD7, PF0)
  // After this call these ports are set
  // as GPIO inputs and can be freely re-programmed to change config.
  // or assign to alternate any functions available on those pins
  PortFunctionInit();

  // Your hardware initialization function calls go here

  // now initialize the Events and Services Framework and start it running
  ErrorType = ES_Initialize(ES_Timer_RATE_1mS);
  if (ErrorType == Success)
  {
    ErrorType = ES_Run();
  }
  //if we got to here, there was an error
  switch (ErrorType)
  {
    case FailedPost:
    {
      printf("Failed on attempt to Post\n");
    }
    break;
    case FailedPointer:
    {
      printf("Failed on NULL pointer\n");
    }
    break;
    case FailedInit:
    {
      printf("Failed Initialization\n");
    }
    break;
    default:
    {
      printf("Other Failure\n");
    }
    break;
  }
  for ( ; ;)
  {
    ;
  }
}
/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/

