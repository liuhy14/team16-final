/****************************************************************************

  Header file for WhackALight.c
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef WhackALight_H
#define WhackALight_H

#include <stdint.h>
#include <stdbool.h>

#include "ES_Events.h"

typedef enum {Welcome, WhackMe, Celebration} WhackALightState_t;
// Public Function Prototypes

bool InitializeWhackALight(uint8_t Priority);
bool PostWhackALight(ES_Event_t ThisEvent);
ES_Event_t RunWhackALightSM(ES_Event_t ThisEvent);
bool CheckWhackALight_Sensor1(void);
bool CheckWhackALight_Sensor2(void);
bool CheckWhackALight_Sensor3(void);
bool CheckWhackALight_Sensor4(void);


#endif /* ServTemplate_H */
