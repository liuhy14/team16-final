

//#define TEST
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>


// the headers to access the GPIO subsystem
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

// the headers to access the TivaWare Library
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"

#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_Timers.h"
#include "ES_Port.h"
#include "ES_Types.h"
#include "termio.h"
#include "EnablePA25_PB23_PD7_PF0.h"

#include "Master.h"
#include "LEAF.h"
#include "Servo.h"
#include "ADMulti.h"

#define ThresholdOfBlack 3000
#define stampUpAngle 120
#define stampDownAngle 30

static bool LastLeafState;
void delay(int number_of_seconds);

//set LastLeafState; set Stamp to up position 
//This should be called in MasterMachine's initialization function
void InitLeaf()
{
  ServoWrite(4,stampUpAngle);
	LastLeafState = ReadLeafState();
}

bool ReadLeafState()
{
  uint32_t results[2];
	ADC_MultiRead(results);
	if(results[1] > ThresholdOfBlack)
		return true;
	else 
		return false;
}

//This should be included in EventCheckers
bool CheckLeafRemoved()
{
  bool CurrentLeafState = ReadLeafState();
	bool returnVal = false;
	if(CurrentLeafState != LastLeafState){
		if(CurrentLeafState == false){
			ES_Event_t ThisEvent;
      ThisEvent.EventType = LEAF_REMOVED;
      PostMaster(ThisEvent);
			returnVal = true;
		}
		LastLeafState = CurrentLeafState;		
	}
	return returnVal;
}

void stamp()
{
	ServoWrite(4,stampDownAngle);
  delay(200);
  ServoWrite(4,stampUpAngle);
  delay(200);
}

void delay(int mseconds)
{
    uint16_t goal = mseconds + ES_Timer_GetTime();
    while (goal > ES_Timer_GetTime());
}


/************
TEST HARNESS
************/

#ifdef TEST

int main(){
  printf("dfb");
  _HW_Timer_Init(ES_Timer_RATE_1mS);
  while(1){
    
    ServoWrite(4,stampDownAngle);
    delay(200);
    ServoWrite(4,stampUpAngle);
    delay(200);
    
  }
   return 0; 
}

#endif