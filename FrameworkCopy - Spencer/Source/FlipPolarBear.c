#include "FlipPolarBear.h"
#include "Servo.h"

#define BearUpAngle 90
#define BearDownAngle 0

void FlipDownBear()
{
	ServoWrite(3,BearDownAngle);
}

void FlipUpBear()
{
  ServoWrite(3,BearUpAngle);
}