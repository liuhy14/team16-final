#ifndef STARTBUTTON_DEBOUNCE_H
#define STARTBUTTON_DEBOUNCE_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */
#include "ES_Events.h"

//Public Function Prototypes
bool InitButtonDebounce(uint8_t Priority);
bool PostButtonDebounce(ES_Event_t ThisEvent);
ES_Event_t RunButtonDebounce(ES_Event_t ThisEvent);
bool CheckStartButton(void);

// typedefs for the states
// State definitions for use with the query function
typedef enum ButtonState{
	Debouncing,
	Ready2Sample
}StartButtonState_t;

#endif //STARTBUTTON_DEBOUNCE_H
