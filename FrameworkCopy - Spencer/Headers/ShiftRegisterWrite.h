// create your own header file comment block
// and protect against multiple inclusions
#ifndef SHIFT_REGISTER_WRITE_H
#define SHIFT_REGISTER_WRITE_H

// the common headers for C99 types 
#include <stdint.h>
#include <stdbool.h>

void SR_Init(void);
uint8_t SR_GetCurrentRegister(void);
void SR_Write(uint8_t NewValue);

#endif //SHIFT_REGISTER_WRITE_H