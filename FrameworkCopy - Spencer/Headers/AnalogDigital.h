/****************************************************************************
AnalogDigital.h
    Module for interfacing AD Inputs
****************************************************************************/

#ifndef AD_H
#define AD_H

#include <stdint.h>
#include <stdbool.h>

void ADInit(void);


#endif