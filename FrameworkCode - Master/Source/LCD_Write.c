/***************************************************************************
 Module
   LCDWrite.c

 Revision
   1.0.1

 Description
   This module acts as the low level interface to an LCD display in 4-bit
   mode with the actual data written to a shift register.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 10/12/15 15:15 jec     first pass

****************************************************************************/
//----------------------------- Include Files -----------------------------*/
//Test Harness
//#define TEST


// the common headers for C99 types
#include <stdint.h>
#include <stdbool.h>

#include "BITDEFS.H"
#include "ShiftRegisterWrite.h"
#include "LCD_Write.h"
#include "ShiftRegisterWrite.h"

// Private functions
static void LCD_SetData4(uint8_t NewData);
static void LCD_PulseEnable(void);
static void LCD_RegisterSelect(uint8_t WhichReg);
static void LCD_Write4(uint8_t NewData);
static void LCD_Write8(uint8_t NewData);

// module level defines
#define LCD_COMMAND 0
#define LCD_DATA    1
#define NUM_INIT_STEPS 10
#define FOUR_BIT_WRITE 0x8000 //Puts 1 an MSB to indicate this is a 4-bit write

// these are the iniitalization values to be written to set up the LCD (AN ARRAY)
static const uint16_t InitValues[NUM_INIT_STEPS] = {
  (0x30>>4)|FOUR_BIT_WRITE,  //0011 0000 -> 0000 0011 (Shifted over to do LCD_WriteCommand4 which uses 4 LSB)
  (0x30>>4)|FOUR_BIT_WRITE,  //0011 0000 -> 0000 0011
  (0x30>>4)|FOUR_BIT_WRITE,  //0011 0000 -> 0000 0011
  (0x20>>4)|FOUR_BIT_WRITE,  //0010 0000 -> 0000 0010
  //Up to this point, device is in 8 bit mode. Now it's in 4-bit mode
  0x002c,  //0010 0100, 4-bit data width, 1 line, 5x7 font
  0x0008,  //0000 1000, turn off the display
  0x0001,  //0000 0001, clear the display
  0x0006,  //0000 0111, increment and shift with each new character
  0x000f,  //0000 1111, turn on display, cursor & blink
  0x0080  //1001 0111, position cursor
  };

// these are the delays between the initialization steps. (AN ARRAY)
// the first delay is the power up delay so there is 1 more entry
// in this table than in the InitValues Table
static const uint16_t InitDelays[NUM_INIT_STEPS+1] = {
    65535, /* use max delay for powerup */
     4100,
      100,
      100,
      100,
       53,
       53,
     3000,
       53,
       53,
       53 };

// place to keep track of which register we are writing to
static uint8_t RegisterSelect=0;

// for keeping track of the initialization step
// 0 is a special case of powerup, then it takes on 1 to NUM_INIT_STEPS
static uint8_t CurrentStep = 0;
       

/****************************************************************************
Function:      LCD_HWInit
Parameters:    None
Returns:       Nothing

Description:Initializes the port hardware necessary to write to the LCD

NOTES:This implementation uses the lower level shift register library so
it simply calls the init for that library

Author:       J. Edward Carryer, 10/12/15, 15:22
****************************************************************************/
void LCD_HWInit(void){
  // init the shift Register module
  SR_Init();
}

/****************************************************************************
Function:       LCD_TakeInitStep
Parameters:     None
Returns:        uint16_t the number of uS to delay before the next step

Description:steps through the initialization value array with each call and send the
initialization value to the LCD and returns the time to delay until the
next step. Return of 0 indicates the process is complete.
Notes

Author:         J. Edward Carryer, 10/12/15, 15:22
****************************************************************************/

uint16_t LCD_TakeInitStep(void){

  uint16_t CurrentInitValue;
  uint16_t Delay;
  uint8_t  NewData; //Downcast Variable for using our LCD_Write functions

  //Module Level Static Variables we will be referencing:
  //static const uint16_t InitDelays[NUM_INIT_STEPS+1]
  //static const uint16_t InitValues[NUM_INIT_STEPS]

  // if we have completed the initialization steps,
  if(CurrentStep > NUM_INIT_STEPS)
  {
    //set Delay value to zero
    Delay = 0;
  }
  else
  {
    // if we are just getting started, this is a special case and we inc
    // the CurrrentStep and set the Delay to the first entry in the
    //InitDelays table
    if(CurrentStep == 0)
    {
      Delay = InitDelays[0];
    }
    else
    {
      // normal step, so grab the correct init value into CurrentInitValue
      // check to see if this is a 4-bit or 8-bit write and do the right kind
      // grab the correct delay for this step
      CurrentInitValue = InitValues[CurrentStep-1];
      Delay            = InitDelays[CurrentStep];

      //ie if the MSB is not set, then we have an 8 bit write
      if((FOUR_BIT_WRITE & CurrentInitValue) == 0)
      {
        NewData = CurrentInitValue;
        LCD_WriteCommand8(NewData);
      }
      //Otherwise we have a 4 bit write
      else
      {
        NewData = CurrentInitValue;
        LCD_WriteCommand4(NewData);
      }
    }
  }

  // set up CurrentStep for next call
  ++CurrentStep;

  return Delay;
}

/****************************************************************************
Function:       LCD_WriteCommand4
Parameters:     uint8_t NewData; the 4 LSBs are written
Returns:        Nothing

Description:clears the register select bit to select the command register then
writes the 4 bits of data, then pulses (raises, then lowers) the enable
line on the LCD

NOTES: This implementation uses the lower level shift register library so
it calls that library to change the value of the pins connected
to the LCD

Author:J. Edward Carryer, 10/12/15, 16:18
****************************************************************************/
void LCD_WriteCommand4(uint8_t NewData){
  // clear the register select bit
  LCD_RegisterSelect(LCD_COMMAND);
  // write the 4 LSBs to the shift register
  LCD_Write4(NewData);
}

/****************************************************************************
Function:         LCD_WriteCommand8
Parameters:       uint8_t NewData;
Returns:          Nothing

Description:clears the register select bit to select the command register then
 writes all 8 bits of data, using 2 4-bit writes

NOTES:This implementation uses the lower level shift register library so
 it calls that library to change the value of the pins connected
 to the LCD

Author:J. Edward Carryer, 10/12/15, 16:26
****************************************************************************/
void LCD_WriteCommand8(uint8_t NewData){
  // clear the register select bit
  LCD_RegisterSelect(LCD_COMMAND);
  // write all 8 data bits to the shift register
  LCD_Write8(NewData);
}

/****************************************************************************
Function:         LCD_WriteData8
Parameters:       uint8_t NewData; 
Returns:          Nothing

Description:sets the register select bit to select the data register then
writes all 8 bits of data, using 2 4-bit writes

NOTES:This implementation uses the lower level shift register library so
it calls that library to change the value of the pins connected
to the LCD

Author:J. Edward Carryer, 10/12/15, 16:28
****************************************************************************/
void LCD_WriteData8(uint8_t NewData){
  // set the register select bit
  LCD_RegisterSelect(LCD_DATA);
  // write all 8 bits to the shift register in 2 4-bit writes
  LCD_Write8(NewData);
}

//********************************
// thses functions are private to the module
//********************************
/****************************************************************************
Function:         LCD_Write4
Parameters:       uint8_t NewData; the 4 LSBs are written
Returns:          Nothing

Description:writes the 4 bits of data, then pulses (raises, then lowers) the enable
line on the LCD

NOTES:This implementation uses the lower level shift register library so
it calls that library to change the value of the pins connected
to the LCD

Author:         J. Edward Carryer, 10/12/15, 15:58
****************************************************************************/
static void LCD_Write4(uint8_t NewData){
  // put the 4 bits of data (4 LSB of NewData) onto the LCD data lines
  LCD_SetData4(NewData);
  // pulse the enable line to complete the write
  LCD_PulseEnable();
}

/****************************************************************************
Function:         LCD_Write8
Parameters:       uint8_t NewData; all 8 bits are written
Returns:          Nothing

Description:writes the 8 bits of data, pulsing the Enable line between the 4 bit writes

NOTES:This implementation uses the lower level shift register library so
it calls that library to change the value of the pins connected
to the LCD

Author:         J. Edward Carryer, 10/12/15, 15:58
****************************************************************************/
static void LCD_Write8(uint8_t NewData){
  uint8_t upper4Bits = 0;
  uint8_t lower4Bits = 0;

  // put the 4 MSBs of data onto the LCD data lines
  upper4Bits = NewData>>4;
  LCD_Write4(upper4Bits);

  // put the 4 LSBs of data onto the LCD data lines
  //(Don't technically have to clear upper 4 bits, but it makes it more clear.)
  lower4Bits = (NewData & (BIT7LO & BIT6LO & BIT5LO & BIT4LO));
  LCD_Write4(lower4Bits);
}

/****************************************************************************
Function:         LCD_RegisterSelect
Parameters:       uint8_t WhichReg; Should be either LCD_COMMAND or LCD_DATA
Returns:          Nothing

Description:sets the port bit that controls the register select to the requested value

NOTES:This implementation uses the lower level shift register library so
 it calls that library to change the value of the register select port bit

Author:         J. Edward Carryer, 10/12/15, 15:28
****************************************************************************/
static void LCD_RegisterSelect(uint8_t WhichReg){
    RegisterSelect = WhichReg;
}

/****************************************************************************
Function:         LCD_SetData4
Parameters:       uint8_t NewData; only the 4 LSBs are used
Returns:          Nothing

Description:sets the 4 data bits to the LCD to the requested value and places the
   register select value in the correct Bit position (bit 1)

NOTES:This implementation uses the lower level shift register library so
   it calls that library to change the value of the data pins

Author:           J. Edward Carryer, 10/12/15, 15:42
****************************************************************************/
static void LCD_SetData4(uint8_t NewData){

  // get the current value of the port so that we can preserve the other bit states
  uint8_t CurrentValue = SR_GetCurrentRegister();

  // insert the current state of RegisterSelect into RS
  uint8_t RS = RegisterSelect;

  //Shift our bit to the second LSB (Since R/S is on output 1 on the shift register)
  RS <<= 1;

  // put the 4 LSBs into the 4 MSB positions to apply the data to the
  // correct LCD inputs while preserving the states of the other bits
  NewData <<= 4;

  //-----Now write the new value to the shift register-----

  //Clear the bits in CurrentValue that we wish to write to, then set the bits
  CurrentValue &= (BIT7LO & BIT6LO & BIT5LO & BIT4LO & BIT1LO);
  CurrentValue |= (NewData | RS);

  //Write CurrentValue back to Shift Register. Only R/S pin has been updated.
  SR_Write(CurrentValue);
}

/****************************************************************************
Function:         LCD_PulseEnable
Parameters:       Nothing
Returns:          Nothing

Description:pulses (raises, then lowers) the enable line on the LCD

Notes:This implementation uses the lower level shift register library so
   it calls that library to change the value of the data pin connected
   to the Enable pin on the LCD (bit 0 on the shift register)

Author:           J. Edward Carryer, 10/12/15, 15:42
****************************************************************************/
static void LCD_PulseEnable(void){

  // get the current value of the port so that we can preserve the other bit states
  uint8_t CurrentValue = SR_GetCurrentRegister();

  // set the LSB of the byte to be written to the shift register
  CurrentValue |= BIT0HI;
  // now write the new value to the shift register
  SR_Write(CurrentValue);

  // clear the LSB of the byte to be written to the shift register
  CurrentValue &= BIT0LO;
  // now write the new value to the shift register
  SR_Write(CurrentValue);

}

void resetCurrentStep()
{
  CurrentStep = 0;
}

/* Test Harness for this module */
#ifdef TEST

#include "termio.h"
int main(void)
{
  // initialize the timer sub-system and console I/O
	TERMIO_Init();
	//clrScrn();

  // When doing testing, it is useful to announce just which program
	// is running.
	puts("\n\rShiftRegisterWrite Test Harness\r");
	printf("%s %s\n",__TIME__, __DATE__);
	printf("\n\r\n");

  LCD_HWInit();
  printf("\rInit done\n");

  //Test General Writing
  if(0)
  {
    LCD_RegisterSelect(LCD_DATA); //LCD_DATA or LCD_COMMAND
    printf("\rRegister Select done\n");

    uint8_t TestData = 0xff;

    LCD_SetData4(TestData);
    LCD_PulseEnable();
    LCD_SetData4(!TestData);

    printf("\rSetData4 and PulseEnable done\n");

    //LCD_Write4(TestData);
    LCD_Write4(TestData);
    LCD_Write4(!TestData);
    LCD_Write4(TestData);
    //LCD_Write4(!TestData);

    printf("\rWrite 4 or 8 done\n");
  }

  //Test LCD Init Steps
  else
  {
    uint16_t Delay = 0;

    for(int i = 0; i<=NUM_INIT_STEPS+1; i++)
    {
      Delay = LCD_TakeInitStep();
      printf("\rDelay is %d\n\r",Delay);
      while(!kbhit())
      {
        //Just Wait
      }
      getchar();

    }

    //Write Characters
    char mychar = 'b';
    printf("\rMychar is %d\n\r",mychar);
    LCD_WriteData8(mychar);
    mychar = 'e';
    LCD_WriteData8(mychar);

    printf("\rProgram Done\n\r");
  }
  for ( ; ;)
  {
    ;
  }

  return 0;
}
#endif
