/****************************************************************************
Servo.c
    Module for interfacing Servos
		
		TODO:
		Interface additional PWM pins if required
****************************************************************************/

//#define TEST


#include <stdint.h>
#include <stdbool.h>

// the headers to access the GPIO subsystem
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

// the headers to access the TivaWare Library
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"

#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_Port.h"
#include "termio.h"
#include "EnablePA25_PB23_PD7_PF0.h"

#define clrScrn() printf("\x1b[2J")
#define goHome() printf("\x1b[1,1H")
#define clrLine() printf("\x1b[K")

// the header for definition of constants
#include "BITDEFS.H"

#include "Servo.h" // the header file for this module

#include "PWM16Tiva.h"

// readability defines


//all belong to PortB
#define BUCKET_SERVO_PIN BIT6HI //Servo1
#define PASSAGE_SERVO_PIN BIT7HI	//Servo2
#define BEAR_SERVO_PIN BIT4HI		//Servo3
#define STAMP_SERVO_PIN BIT5HI	//SErvo4

#define NO_OF_SERVOS 4


// in ticks (0.8us x thisvalue)

#define BUCKET_180_PULSE 3000
#define PASSAGE_180_PULSE 3000
#define BEAR_180_PULSE 3000
#define STAMP_180_PULSE 3000

#define ANGLE_0_PULSE_DEFAULT 1100

// Varun's calibraio

#define ANGLE_0_PULSE_BUCKET 1100 //calibrated 6001HB     
#define ANGLE_RANGE_BUCKET 1550 //6001HB                  

// Alan's calbration:

#define ANGLE_0_PULSE_BEAR 1100 //15178HB 
#define ANGLE_RANGE_BEAR 2200

#define ANGLE_0_PULSE_PoT 1100//HK15138
#define ANGLE_RANGE_PoT 2200

#define ANGLE_0_PULSE_STAMP 1100 //HS322
#define ANGLE_RANGE_STAMP 2200





// symbolic constant to read or write all bits at once
#define ALL_BITS (0xff<<2)

/****************************************************************************
 Function
    ServoInit
 Parameters
    None
 Returns
    Nothing
 Description
    Initializes the shift register
****************************************************************************/

//initializes Servos
//IMP: You must send it to home position in your respective modules for Welcome/other state(s)
//this will be called in master  init function
void ServoInit(void)
{
    // enable the system clock into Port B
    HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R1;

    // wait until the peripheral reports its clock as ready
    while ((HWREG(SYSCTL_PRGPIO) & SYSCTL_PRGPIO_R1) != SYSCTL_PRGPIO_R1)
    {
        // don't do anyting till the peripheral clock is ready
    }

    // set servo pins as digital I/O
    HWREG(GPIO_PORTB_BASE+GPIO_O_DEN) |= ( BUCKET_SERVO_PIN | PASSAGE_SERVO_PIN | BEAR_SERVO_PIN | STAMP_SERVO_PIN );

    // program servo pins as output
    HWREG(GPIO_PORTB_BASE+GPIO_O_DIR) |= ( BUCKET_SERVO_PIN | PASSAGE_SERVO_PIN | BEAR_SERVO_PIN | STAMP_SERVO_PIN );

    // intitialize four channels on the PWM library
    PWM_TIVA_Init( NO_OF_SERVOS );

    // set period of all four channels to 20ms
    PWM_TIVA_SetPeriod( 25000, 0 );	//group 0 channels 0 and 1
    PWM_TIVA_SetPeriod( 25000, 1 );	//group 1 channels 2 and 3 
    

}



//ServoNum can be 1,2,3,4
//Angle between 0 and 180
void ServoWrite( uint8_t ServoNum, uint16_t Angle)
{
		
		uint16_t ChannelNum = ServoNum-1;	//maps 1,2,3,4 to 0,1,2,3 channel numbers
	
    // if angle is less than or equal to 0 deg
    if ( Angle <= 0 )
    {
        // write the 0 degree to the corresponding servo
        PWM_TIVA_SetPulseWidth( ANGLE_0_PULSE_DEFAULT , ChannelNum );
    }
    // if angle is greater than or equal to 180 degrees
    else if ( Angle >= 180 )
    {
        // move the servo to 180 degrees
        if (ServoNum == 1)
        {
            PWM_TIVA_SetPulseWidth( BUCKET_180_PULSE , ChannelNum );
        }
         if (ServoNum == 2)
        {
            PWM_TIVA_SetPulseWidth( BEAR_180_PULSE , ChannelNum );
        }
         if (ServoNum == 3)
        {
            PWM_TIVA_SetPulseWidth( BEAR_180_PULSE , ChannelNum );
        }
				 if (ServoNum == 4)
        {
            PWM_TIVA_SetPulseWidth( STAMP_180_PULSE , ChannelNum );
        }
    }
    // else the angle lies between 0 and 180 degrees
    else
    {
        // choose pulse width for the servo depending on ServoNum and write angle
        
        if (ServoNum == 1)
        { 
          
            PWM_TIVA_SetPulseWidth( ANGLE_0_PULSE_BUCKET + ( ANGLE_RANGE_BUCKET * Angle / 180.0 ) , ChannelNum);	//calibrate these values!!!
         
        }
        if (ServoNum == 2)
        {
            PWM_TIVA_SetPulseWidth( ANGLE_0_PULSE_PoT + ( ANGLE_RANGE_PoT * Angle / 180.0 ) , ChannelNum);	//calibrate these values!!!
          //puts("came here \n \r");
        }
       if (ServoNum == 3)
        {
            PWM_TIVA_SetPulseWidth( ANGLE_0_PULSE_BEAR + ( ANGLE_RANGE_BEAR * Angle / 180.0 ) , ChannelNum);	//calibrate these values!!!
        }
				if (ServoNum == 4)
        {
            PWM_TIVA_SetPulseWidth( ANGLE_0_PULSE_STAMP + ( ANGLE_RANGE_STAMP * Angle / 180.0 ) , ChannelNum);	//calibrate these values!!!
        }
			}
}




/*************************
      TEST HARNESS
*************************/

/* TEST HARNESS */

#ifdef TEST
/*testharness*/

int main(void)
{
  
  SysCtlClockSet(SYSCTL_SYSDIV_5 | SYSCTL_USE_PLL | SYSCTL_OSC_MAIN | SYSCTL_XTAL_16MHZ);
  TERMIO_Init();
  
  printf("Inside main \n\r");
  
  //uint8_t CurrentInputState = 0;
  ES_Initialize(ES_Timer_RATE_1mS);
  
  ServoInit();
	
	while(1)
	{
		ServoWrite(1,90);
    puts("writing \n\r");
		//delay(10000);
		//ServoWrite(1,135);
		//delay(10000);
  }
     
  
  
  return 0;
}


#endif