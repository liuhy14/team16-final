#include "FlipPolarBear.h"
#include "Servo.h"

#define BearUpAngle 120
#define BearDownAngle 20

void FlipDownBear()
{
	ServoWrite(3,BearDownAngle);
}

void FlipUpBear()
{
  ServoWrite(3,BearUpAngle);
}