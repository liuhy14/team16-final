/****************************************************************************
 Module
   TemplateFSM.c

 Revision
   1.0.1

 Description
   This is a template file for implementing flat state machines under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/15/12 11:12 jec      revisions for Gen2 framework
 11/07/11 11:26 jec      made the queue static
 10/30/11 17:59 jec      fixed references to CurrentEvent in RunTemplateSM()
 10/23/11 18:20 jec      began conversion from SMTemplate.c (02/20/07 rev)
 
 / TODO access to posting points to write to LCD/Screen
// TODO access to trigger specific sounds on audio board
****************************************************************************/


/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "Points.h"
#include "Ports.h"
#include "LCDService.h"
#include <stdio.h>


/*----------------------------- Module Defines ----------------------------*/

//Pin defines
#define SOUND_1_PIN GPIO_PIN_3 //PA3
#define SOUND_1_HI BIT3HI
#define SOUND_1_LO BIT3LO

#define SOUND_2_PIN GPIO_PIN_4 //PA4
#define SOUND_2_HI BIT4HI
#define SOUND_2_LO BIT4LO

#define SOUND_3_PIN GPIO_PIN_5 //PA5
#define SOUND_3_HI BIT5HI
#define SOUND_3_LO BIT5LO

#define SOUND_4_PIN GPIO_PIN_6 //PA6
#define SOUND_4_HI BIT6HI
#define SOUND_4_LO BIT6LO

#define SOUND_5_PIN GPIO_PIN_7 //PA7
#define SOUND_5_HI BIT7HI
#define SOUND_5_LO BIT7LO

//Map pins to sounds
#define GAME_START_SOUND  SOUND_1_PIN 
#define BASKET_SOUND      SOUND_2_PIN 
#define RAIN_SOUND        SOUND_3_PIN 
#define LIGHT_SOUND       SOUND_4_PIN 
#define GAME_OVER_SOUND   SOUND_5_PIN 

#define SOUND_TIMER 13
#define SOUND_DELAY 200

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this machine.They should be functions
   relevant to the behavior of this state machine
*/
static void PulseTrigger(uint8_t trigger_pin);

/*---------------------------- Module Variables ---------------------------*/
// everybody needs a state variable, you may need others as well.
// type of state variable should match htat of enum in header file


// with the introduction of Gen2, we need a module level Priority var as well
static uint8_t MyPriority;
static uint32_t Points = 0;	//stores the points 

/*------------------------------ Module Code ------------------------------*/

/****************************************************************************
Function:     InitPoints
Parameters:   uint8_t : the priorty of this service
Returns:      bool, false if error in initialization, true otherwise
Description:  Saves away the priority, sets up the initial transition and does any
              other required initialization for this state machine
****************************************************************************/
bool InitPoints(uint8_t Priority)
{
  ES_Event_t ThisEvent;
  MyPriority = Priority;

  //Initialize the pins for sound triggering
  SetPin2Digital_PortA(SOUND_1_PIN | SOUND_2_PIN | SOUND_3_PIN | SOUND_4_PIN | SOUND_5_PIN);
  SetPin2Output_PortA(SOUND_1_PIN | SOUND_2_PIN | SOUND_3_PIN | SOUND_4_PIN | SOUND_5_PIN);
	
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService( MyPriority, ThisEvent) == true)
  {
    return true;
  }else
  {
    return false;
  }
}

/****************************************************************************
Function:     PostPoints
Parameters:   EF_Event ThisEvent , the event to post to the queue
Returns:      boolean False if the Enqueue operation failed, True otherwise
Description:  Posts an event to this state machine's queue
****************************************************************************/
bool PostPoints(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
Function:     RunPoints
Parameters:   ES_Event : the event to process
Returns:      ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise
Notes:        Uses nested switch/case to implement the machine.
****************************************************************************/
#include "termio.h"

ES_Event_t RunPoints(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
	uint32_t NoOfPoints;
	
	if(ThisEvent.EventType == POST_POINTS)
	{
    printf("\rPoints Posted\n\r");
		NoOfPoints = ThisEvent.EventParam;
		Points = Points + NoOfPoints;		
		
		//POST TO LCD SM1
		if(NoOfPoints == BASKET_POINTSVAL)
		{
			//TRIGGER BASKET BALL SOUND
      //Pull line low
      PORTA &= ~BASKET_SOUND;
      //Set Timer
      ES_Timer_InitTimer(SOUND_TIMER, SOUND_DELAY);\
      //Write Points to LCD
      char str[20];
      sprintf(str, "Points: %d        ", Points);
      WriteString2LCD(str);
		}
		if(NoOfPoints == RAIN_POINTSVAL)
		{
			//TRIGGER RAIN SOUND
       PORTA &= ~RAIN_SOUND;
      //Set Timer
      ES_Timer_InitTimer(SOUND_TIMER, SOUND_DELAY);
      //Write Points to LCD
      char str[20];
      sprintf(str, "Points: %d        ", Points);
      WriteString2LCD(str);
		}
		if(NoOfPoints == LIGHT_POINTSVAL)
		{
			//TRIGGER LIGHT SOUND
      PORTA &= ~LIGHT_SOUND; 
      //Set Timer
      ES_Timer_InitTimer(SOUND_TIMER, SOUND_DELAY);
      //Write Points to LCD
      char str[20];
      sprintf(str, "Points: %d        ", Points);
      WriteString2LCD(str);
		}		
	}
	if(ThisEvent.EventType == RESET)
	{
    //Reset the points
		Points =0;
    //Clear the screen
    char str[] = "                         ";
    WriteString2LCD(str);
	}
  if(ThisEvent.EventType == GAME_START)
	{
    //TRIGGER Game start sound
    PORTA &= ~GAME_START_SOUND; 
    //Set Timer
    ES_Timer_InitTimer(SOUND_TIMER, SOUND_DELAY);
    //Write "Game Start!" to LCD
    char str[] = "Game Start!              ";
    WriteString2LCD(str);
  }
  if(ThisEvent.EventType == GAME_OVER)
	{
    //Reset the points
		//Points =0;
		//TRIGGER game over sound (celebration sound)
    PORTA &= ~GAME_OVER_SOUND; 
    //Set Timer
    ES_Timer_InitTimer(SOUND_TIMER, SOUND_DELAY);
    //Write "Polar Bears Win!" to LCD
    char str[] = "Polar Bears Win!";
    char str2[] = "Polar Bears Lose!";
    ClearLCD();
    if(Points<30)WriteString2LCD(str2);
    else WriteString2LCD(str);
	}
  if( ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == SOUND_TIMER)
  {
    //Set all lines high
    PORTA |= (BASKET_SOUND | RAIN_SOUND | LIGHT_SOUND | GAME_START_SOUND | GAME_OVER_SOUND);
  }
  printf("\rPoints are %d\n\r", Points);
  return ReturnEvent;
}

/****************************************************************************
Function:     GetPoints for other modules to access this
Parameters:   None
Returns:      Points uint32_t

****************************************************************************/
uint32_t GetPoints(void)
{
  return Points;
}

/***************************************************************************
 private functions
 ***************************************************************************/
static void PulseTrigger(uint8_t trigger_pin)
{
  //Receives a BITXHI variable. 
  
  //Pull line low
  PORTA &= ~trigger_pin;
}

/*
static *char convert2String(uint32_t Points)
{
  
  
  
}
*/
