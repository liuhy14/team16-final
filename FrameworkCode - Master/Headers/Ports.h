#ifndef PORT_A_H
#define PORT_A_H

//Header Files needed to make HWREG work, and BITDEFS
//include header files for hardware access
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

// the common headers for C99 types
#include <stdint.h>
#include <stdbool.h>

// the headers to access the TivaWare Library
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"

#include "BITDEFS.H"

//Public Function Definitions

//Enable all the Ports
void EnableAllPorts();

//Accessibility functions for Port A pins
void SetPin2Digital_PortA(uint8_t PINS);
void SetPin2Output_PortA(uint8_t PINS);
void SetPin2Input_PortA(uint8_t PINS);

//Accessibility functions for Port B pins
void SetPin2Digital_PortB(uint8_t PINS);
void SetPin2Output_PortB(uint8_t PINS);
void SetPin2Input_PortB(uint8_t PINS);

//Accessibility functions for Port D pins
void SetPin2Digital_PortD(uint8_t PINS);
void SetPin2Output_PortD(uint8_t PINS);
void SetPin2Input_PortD(uint8_t PINS);

//Defines
#define ALL_BITS (0xff<<2)
#define PORTA   (HWREG(GPIO_PORTA_BASE+(GPIO_O_DATA+ALL_BITS)))
#define PORTB   (HWREG(GPIO_PORTB_BASE+(GPIO_O_DATA+ALL_BITS)))
#define PORTC   (HWREG(GPIO_PORTC_BASE+(GPIO_O_DATA+ALL_BITS)))
#define PORTD   (HWREG(GPIO_PORTD_BASE+(GPIO_O_DATA+ALL_BITS)))
#define PORTE   (HWREG(GPIO_PORTE_BASE+(GPIO_O_DATA+ALL_BITS)))
#define PORTF   (HWREG(GPIO_PORTF_BASE+(GPIO_O_DATA+ALL_BITS)))

#endif //PORT_A_H
