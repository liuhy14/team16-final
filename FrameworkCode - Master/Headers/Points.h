/****************************************************************************

  Header file for Points.c
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef POINTS_H
#define POINTS_H

#include <stdint.h>
#include <stdbool.h>

#include "ES_Events.h"

//defines for other modules that post to the service
#define BASKET_POINTSVAL 2
#define RAIN_POINTSVAL 3
#define LIGHT_POINTSVAL 1

// Public Function Prototypes
bool InitPoints(uint8_t Priority);
bool PostPoints(ES_Event_t ThisEvent);
ES_Event_t RunPoints(ES_Event_t ThisEvent);
uint32_t GetPoints(void);

#endif //POINTS_H