#ifndef BASKET_H
#define BASKET_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */
#include "ES_Events.h"

// typedefs for the states
// State definitions for use with the query function
typedef enum BucketState{
  InitBasket,
  Welcome,
  Playing,
  Celebration
}BucketState_t;

//Public Function Prototypes
bool       InitBucket(uint8_t Priority)
bool       PostBucket(ES_Event_t ThisEvent);
ES_Event_t RunBucket (ES_Event_t ThisEvent);

#endif //BASKET_H
