/*----------------------------- Include Files -----------------------------*/
//include header files for this service
#include "BasketLights.h"

//include header files for hardware access
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

//Header for Timing Functions
#include "ES_Port.h"

//include header files for the framework
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"
#include "ES_ShortTimer.h"

//include header files for the other modules that are referenced
#include "Basket.h"

//Include Port B functions
#include "PortB.h"

/*----------------------------- Module Defines ----------------------------*/
#define BASKETLIGHTS_PIN GPIO_PIN_3 //????
#define BASKETLIGHTS_LO  BIT3LO //????
#define BASKETLIGHTS_HI  BIT3HI //????

#define BASKET_TIMER 1
#define WELCOME_TIME 1000
#define CELEBRATION_TIME 1000
#define SWITCH_STATES_TIME 1
#define BASKET_SWITCH_TIME 100
#define BASKET_TIME 1000


/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
relevant to the behavior of this service. */

/*---------------------------- Module Variables ---------------------------*/
//-with the introduction of Gen2, we need a module level Priority variable
//-Data private to the module:

static BasketLightsState_t CurrentState
static BasketState_t CurrentBasketState
static uint8_t counter;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
Function:       InitMorseElements
Parameters:     uint8_t : the priorty of this service
Returns:        bool, false if error in initialization, true otherwise
Description:    Saves away the priority, and does any other required initialization
                for this service
****************************************************************************/

bool InitBasketLights (uint8_t Priority)
{
  ES_Event_t ThisEvent;
  MyPriority = Priority;

  //Initialize the port line to receive beam break signals
  SetPin2Digital(BASKETLIGHTS_PIN);
  SetPin2Input(BASKETLIGHTS_PIN);

  //Set Basket lights to LOW
  PORTB &= BASKETLIGHTS_LO;

  //Set CurrentState to be InitBasket
  CurrentState = InitBasketLights;

  //Post Event ES_Init to MorseElements queue (this service)
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService( MyPriority, ThisEvent) == true)
  {
    return true;
  }else
  {
    return false;
  }
}

/****************************************************************************
Function:     PostLCDService
Parameters:   EF_Event ThisEvent ,the event to post to the queue
Returns:      bool false if the Enqueue operation failed, true otherwise
Description:  Posts an event to this state machine's queue
Author:       J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostBasketLights (ES_Event_t ThisEvent)
{
  return ES_PostToService( MyPriority, ThisEvent);
}

/****************************************************************************
Function:     RunBasket
Parameters:   ES_Event_t : the event to process
Returns:      ES_Event_t, ES_NO_EVENT if no error ES_ERROR otherwise
Description:  State Machine for basket
****************************************************************************/
ES_Event_t RunBasketLights (ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
  BasketLightsState_t NextState = CurrentState; //Initialize NextState to CurrentState

  switch (CurrentState)
  {
    case InitBasketLights :
      if ( ThisEvent.EventType == ES_INIT )
      {
        //Set state to LightsOff
        NextState = LightsOff;
      }
      break;
    case LightsOff :
      //If we detect a BASKET_LIGHTS event
      if ( ThisEvent.EventType == BASKET_LIGHTS )
      {
        //Get the current state of the Basket SM
        CurrentBasketState = GetBasketState();
        //Set the switching timer and set the increment variable
        //according to what state the basket SM is in
        if(CurrentBasketState == Welcome)
        {
          ES_Timer_InitTimer(BASKET_TIMER, WELCOME_TIME);
        }
        else if(CurrentBasketState == Playing)
        {
          ES_Timer_InitTimer(BASKET_TIMER, BASKET_TIME);
          Counter = BASKET_TIME/BASKET_SWITCH_TIME;
        }
        else if(CurrentBasketState == Celebration)
        {
          ES_Timer_InitTimer(BASKET_TIMER, CELEBRATION_TIME);
        }
        NextState = LightsOff;
      }
      //If we recieve a Timout event
      if (ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == BASKET_TIMER)
      {
        if(CurrentBasketState == Welcome)
        {
          ES_Timer_InitTimer(BASKET_TIMER, WELCOME_TIME);
          //Turn the Lights on
          PORTB &= BASKETLIGHTS_HI;
          //Change state variable
          NextState = LightsOn;
          //Set the Timer again
        }
        else if(CurrentBasketState == Playing)
        {
          //If the counter is zero, we stop flashing
          if(counter > 0)
          {
            //Decrement the counter
            counter--;
            //Set the timer again
            ES_Timer_InitTimer(BASKET_TIMER, BASKET_TIME);
            //Turn the Lights on
            PORTB &= BASKETLIGHTS_HI;
            //Change state variable
            NextState = LightsOn;
          }
          else
          {
            NextState = LightsOff;
          }
        }
        else if(CurrentBasketState == Celebration)
        {
          ES_Timer_InitTimer(BASKET_TIMER, CELEBRATION_TIME);
          //Turn the Lights on
          PORTB &= BASKETLIGHTS_HI;
          //Change state variable
          NextState = LightsOn;
          //Set the Timer again
        }
      }
      //If we detect a RESET event (from the basket SM)
      if ( ThisEvent.EventType == RESET )
      {
        //Set the timer with a short time
        ES_Timer_InitTimer(BASKET_TIMER, SHORT_SWITCH_TIME);
        //Set next state to LightsOn
        nextState = LightsOn;
        /*This will do the following faster than the user can detect: Change to
        LightsOn, a timeout will occur, and we will switch back to LightsOff
        thereby ensuring the lights are off and the timer is not set.*/
      }
      break;
    case LightsOn:
    //If we detect a BASKET_LIGHTS event
    if ( ThisEvent.EventType == BASKET_LIGHTS )
    {
      //Get the current state of the Basket SM
      CurrentBasketState = GetBasketState();
      //Set the switching timer and set the increment variable
      //according to what state the basket SM is in
      if(CurrentBasketState == Welcome)
      {
        ES_Timer_InitTimer(BASKET_TIMER, WELCOME_TIME);
      }
      else if(CurrentBasketState == Playing)
      {
        ES_Timer_InitTimer(BASKET_TIMER, BASKET_TIME);
        Counter = BASKET_TIME/BASKET_SWITCH_TIME;
      }
      else if(CurrentBasketState == Celebration)
      {
        ES_Timer_InitTimer(BASKET_TIMER, CELEBRATION_TIME);
      }
      //Turn off the basket lights
      PORTB &= BASKETLIGHTS_LO;
      //Change to LightsOff state
      NextState = LightsOff;
    }
    //If we recieve a Timout event
    if (ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == BASKET_TIMER)
    {
      if(CurrentBasketState == Welcome)
      {
        //Set the Timer
        ES_Timer_InitTimer(BASKET_TIMER, WELCOME_TIME);
      }
      else if(CurrentBasketState == Playing)
      {
        //If the counter greater than zero, we set the timer again. Otherwise
        //we do nothing here
        if(counter > 0)
        {
          //Decrement the counter
          counter--;
          //Set the timer
          ES_Timer_InitTimer(BASKET_TIMER, BASKET_TIME);
        }
      }
      else if(CurrentBasketState == Celebration)
      {
        //Set the timer
        ES_Timer_InitTimer(BASKET_TIMER, CELEBRATION_TIME);
      }
      //Turn the Lights off
      PORTB &= BASKETLIGHTS_LO;
      //Change state variable
      NextState = LightsOff;
    }
    //If we detect a RESET event (from the basket SM)
    if ( ThisEvent.EventType == RESET )
    {
      //Set the timer with a short time
        ES_Timer_InitTimer(BASKET_TIMER, SHORT_SWITCH_TIME);
      //Set next state to LightsOn
      nextState = LightsOn;
      /*This will do the following faster than the user can detect: Change to
      LightsOn, a timeout will occur, and we will switch back to LightsOff
      thereby ensuring the lights are off and the timer is not set.*/
    }
    break;
  }
  CurrentState = NextState; //Update CurrentState
  return ReturnEvent;
}
