#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */
#include "ES_Events.h"   

typedef enum { Raising, Welcome, Falling, Celebration } POTState_t ;