/****************************************************************************

  Header file for Test Harness Service0
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef BASKET_H
#define BASKET_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */
#include "ES_Events.h"

// typedefs for the states
// State definitions for use with the query function
typedef enum {
  WelcomeLightsOff,
  WelcomeLightsOn,
  PlayingLightsOff,
  PlayingLightsOn,
  CelebrationLightsOff,
  CelebrationLightsOn
} BasketState_t ;

// Public Function Prototypes
bool       InitBasket (uint8_t Priority);
bool       PostBasket (ES_Event_t ThisEvent);
ES_Event_t RunBasket (ES_Event_t ThisEvent);

#endif /* BASKET_H */
