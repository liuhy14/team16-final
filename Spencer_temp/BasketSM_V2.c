/*----------------------------- Include Files -----------------------------*/
//include header files for this service
#include "BasketSM.h"

//include header files for hardware access
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

//Header for Timing Functions
#include "ES_Port.h"

//include header files for the framework
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"
#include "ES_ShortTimer.h"

//include header files for the other modules that are referenced
#include "MorseDecode.h"

//Include Port B functions
#include "PortB.h"

/*----------------------------- Module Defines ----------------------------*/
#define BEAMBREAK_PIN GPIO_PIN_3 //????
#define BEAM_LO  BIT3LO //????
#define BEAM_HI  BIT3HI //????

#define BASKETLIGHTS_PIN GPIO_PIN_3 //????
#define BASKETLIGHTS_LO  BIT3LO //????
#define BASKETLIGHTS_HI  BIT3HI //????

#define BASKET_TIMER 1
#define WELCOME_TIME 100
#define BASKET_TIME 100
#define CELEBRATION_TIME 100
#define SWITCH_STATES_TIME 1


/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
relevant to the behavior of this service. */

/*---------------------------- Module Variables ---------------------------*/
//-with the introduction of Gen2, we need a module level Priority variable
//-Data private to the module:

static BasketState_t CurrentState
char LastInputState;
static uint8_t counter;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
Function:       InitMorseElements
Parameters:     uint8_t : the priorty of this service
Returns:        bool, false if error in initialization, true otherwise
Description:    Saves away the priority, and does any other required initialization
                for this service
****************************************************************************/

bool InitBasket (uint8_t Priority)
{
  ES_Event_t ThisEvent;
  MyPriority = Priority;

  //Initialize the port line to receive beam break signals
  SetPin2Digital(BEAMBREAK_PIN);
  SetPin2Input(BEAMBREAK_PIN);

  //Sample port line and use it to initialize the LastInputState variable
  LastInputState = (PORTB & BEAMBREAK_PIN);

  //Set CurrentState to be InitBasket
  CurrentState = InitBasket;

  //Post Event ES_Init to MorseElements queue (this service)
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService( MyPriority, ThisEvent) == true)
  {
    return true;
  }else
  {
    return false;
  }
}

/****************************************************************************
Function:     PostLCDService
Parameters:   EF_Event ThisEvent ,the event to post to the queue
Returns:      bool false if the Enqueue operation failed, true otherwise
Description:  Posts an event to this state machine's queue
Author:       J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostBasket (ES_Event_t ThisEvent)
{
  return ES_PostToService( MyPriority, ThisEvent);
}

/****************************************************************************
Function:     RunBasket
Parameters:   ES_Event_t : the event to process
Returns:      ES_Event_t, ES_NO_EVENT if no error ES_ERROR otherwise
Description:  State Machine for basket
****************************************************************************/
ES_Event_t RunBasket (ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
  MorseState_t NextState = CurrentState; //Initialize NextState to CurrentState

  switch (CurrentState)
  {
    case InitBasket :
      if ( ThisEvent.EventType == ES_INIT )
      {
        //Set state to WelcomeLights off
        NextState = WelcomeLightsOff;
        //Set timer for basket lights
        ES_Timer_InitTimer(BASKET_TIMER, WELCOME_TIME);
      }
      break;
    case WelcomeLightsOff :
      //If the master SM sends a PLAYING event, switch to PlayingLightsOn
      if ( ThisEvent.EventType == PLAYING )
      {
        //Even though the lights aren't on, switch to this state
        NextState = PlayingLightsOn;
        //Set a short time so we can quickly transistion to PlayingLightsOff
        ES_Timer_InitTimer(BASKET_TIMER, SWITCH_STATES_TIME);
        /*This will do the following faster than the user can detect: Change to
        PlayingLightsOn, a timeout will occur, and we will switch back to
        PlayingLightsOff, thereby ensuring the lights are off and the timer is
        not set.*/
      }
      break;
      //Timeout Event
      if( ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == BASKET_TIMER)
      {
        //Turn Lights on
        PORTB &= BASKETLIGHTS_HI;
        //Reset Timer
        ES_Timer_InitTimer(BASKET_TIMER, WELCOME_TIME);
        //Switch to WelcomeLightsOn
        NextState = WelcomeLightsOn;
      }
    case WelcomeLightsOn :
      //If the master SM sends a PLAYING event, switch to PlayingLightsOn
      if ( ThisEvent.EventType == PLAYING )
      {
        //Even though the lights aren't on, switch to this state
        NextState = PlayingLightsOn;
        //Set a short time so we can quickly transistion to PlayingLightsOff
        ES_Timer_InitTimer(BASKET_TIMER, SWITCH_STATES_TIME);
        /*This will do the following faster than the user can detect: Change to
        PlayingLightsOn, a timeout will occur, and we will switch back to
        PlayingLightsOff, thereby ensuring the lights are off and the timer is
        not set.*/
      }
      break;
      //Timeout Event
      if( ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == BASKET_TIMER)
      {
        //Turn Lights off
        PORTB &= BASKETLIGHTS_LO;
        //Reset Timer
        ES_Timer_InitTimer(BASKET_TIMER, WELCOME_TIME);
        //Switch to WelcomeLightsOff
        NextState = WelcomeLightsOff;
      }
    case PlayingLightsOff :
      //If a BASKET_MADE event is detected
      if ( ThisEvent.EventType == BASKET_MADE )
      {
        //Send a point to point SM

        //Set Counter variable
        Counter = BASKET_TIME/BASKET_SWITCH_TIME;
        //Set Timer
        ES_Timer_InitTimer(BASKET_TIMER, BASKET_TIME);
        //Preserve current state
        NextState = PlayingLightsOff;
      }
      if( ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == BASKET_TIMER)
      {
        //If the counter is greater than zero, we continue flashing
        if(counter > 0)
        {
          //Decrement the counter
          counter--;
          //Set the timer again
          ES_Timer_InitTimer(BASKET_TIMER, BASKET_TIME);
          //Turn the Lights on
          PORTB &= BASKETLIGHTS_HI;
          //Change state variable
          NextState = PlayingLightsOn;
        }
        //Otherwise we stop flashing (ie don't turn lights or timer back on)
        else
        {
          NextState = PlayingLightsOff;
        }
      }
      //If the master SM sends a RESET event, switch back to Welcome mode
      if ( ThisEvent.EventType == RESET )
      {
        //Change back to welcome mode
        NextState = WelcomeLightsOff;
        //Set timer for welcome mode flashing
        ES_Timer_InitTimer(BASKET_TIMER, WELCOME_TIME);
      }
      //If the master SM sends a CELEBRATION event, switch to celebration mode
      if ( ThisEvent.EventType == CELEBRATION )
      {
        NextState = CelebrationLightsOff;
      }
      break;
    case PlayingLightsOn :
      //If a BASKET_MADE event is detected
      if ( ThisEvent.EventType == BASKET_MADE )
      {
        //Send a point to point SM

        //Set Counter variable
        Counter = BASKET_TIME/BASKET_SWITCH_TIME;
        //Set Timer
        ES_Timer_InitTimer(BASKET_TIMER, BASKET_TIME);
      }
      if ( ThisEvent.EventType == ES_INIT )
      {
        NextState = ;
      }
      if( ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == BASKET_TIMER)
      {

      }
      break;
    case CelebrationLightsOff :

      break;
    case CelebrationLightsOn :
      break;
  }
  CurrentState = NextState; //Update CurrentState
  return ReturnEvent;
}

/****************************************************************************
Function: 	  CheckBasket
Parameters: 	None
Returns: 	    bool: true if a new event was detected
Description:	Checks for falling and rising edges on the Basket Pins
Notes:
****************************************************************************/
bool CheckBasket(void)
{
  uint8_t         CurrentInputState;
  bool            ReturnVal = false; //Assume no event has occurred

  //Get the CurrentInputState from the input line
  CurrentInputState = (PORTB & BEAMBREAK_PIN);

  if ((CurrentInputState != LastInputState)
  {
    ES_Event_t ThisEvent; //An event has occured
    if(CurrentInputState == 0)//If the current state of the input line is low
    {
      //A basket had been made. Post event to BasketSM
      ThisEvent.EventType = BASKET_MADE
      PostBasket(ThisEvent)
    }
  ReturnVal = true;
  }
  LastInputState = CurrentInputState; // update the state for next time //[x]

return ReturnVal;
}

/*NOTES:
- BASKET_MADE event added

*/
/********************************END OF FILE**************************************/
