/****************************************************************************
 Module: PortA.c
 Revision 1.0.1
 Description: This module includes basic functions for manipulating pins on PortA
 on Port B
 Notes:
 History
 When           Who     What/Why
 -------------- ---     --------
 10/11/15 19:55 jec     First Creation

 ****************************************************************************/

/*----------------------------- Include Files -----------------------------*/
//include header files for this c file
#include "Ports.h"


//Private function prototypes
static void EnablePortA();
static void EnablePortB();
static void EnablePortC();
static void EnablePortD();
static void EnablePortE();
static void EnablePortF();

//All function defitions

void EnableAllPorts()
{
  EnablePortA();
  EnablePortB();
  EnablePortC();
  EnablePortD();
  EnablePortE();
  EnablePortF();
}

/*Initialize Port A*/
static void EnablePortA()
{
  //Enable Port A
  HWREG(SYSCTL_RCGCGPIO) |= BIT0HI;
  //Stall for a few clock cycle
  while ((HWREG(SYSCTL_PRGPIO) & BIT0HI) != BIT0HI);
  {
    //Wait until port initializes
  }
}

/*Initialize Port B*/
static void EnablePortB()
{
  //Enable Port B
  HWREG(SYSCTL_RCGCGPIO) |= BIT1HI;
  //Stall for a few clock cycle
  while ((HWREG(SYSCTL_PRGPIO) & BIT1HI) != BIT1HI);
  {
    //Wait until port initializes
  }
}
/*Initialize Port C*/
static void EnablePortC()
{
  //Enable Port C
  HWREG(SYSCTL_RCGCGPIO) |= BIT2HI;
  //Stall for a few clock cycle
  while ((HWREG(SYSCTL_PRGPIO) & BIT2HI) != BIT2HI);
  {
    //Wait until port initializes
  }
}
/*Initialize Port D*/
static void EnablePortD()
{
  //Enable Port D
  HWREG(SYSCTL_RCGCGPIO) |= BIT3HI;
  //Stall for a few clock cycle
  while ((HWREG(SYSCTL_PRGPIO) & BIT3HI) != BIT3HI);
  {
    //Wait until port initializes
  }
}
/*Initialize Port E*/
static void EnablePortE()
{
  //Enable Port E
  HWREG(SYSCTL_RCGCGPIO) |= BIT4HI;
  //Stall for a few clock cycle
  while ((HWREG(SYSCTL_PRGPIO) & BIT4HI) != BIT4HI);
  {
    //Wait until port initializes
  }
}
/*Initialize Port F*/
static void EnablePortF()
{
  //Enable Port F
  HWREG(SYSCTL_RCGCGPIO) |= BIT5HI;
  //Stall for a few clock cycle
  while ((HWREG(SYSCTL_PRGPIO) & BIT5HI) != BIT5HI);
  {
    //Wait until port initializes
  }
}

//************************************************************

/*Sets pins corresponding to high bits in "PINS" to
Digital.*/
void SetPin2Digital_PortA(uint8_t PINS)
{
  HWREG(GPIO_PORTA_BASE+GPIO_O_DEN) |= PINS;
}

/*Sets pins corresponding to high bits in "PINS" to
be output pins. A pin is an output if the corresponding
bit in the register is HIGH*/
void SetPin2Output_PortA(uint8_t PINS)
{
  HWREG(GPIO_PORTA_BASE+GPIO_O_DIR) |= PINS;
}

/*Sets pins corresponding to high bits in "PINS" to
be input pins. A pin is an input if the corresponding
bit in the register is LOW. */
void SetPin2Input_PortA(uint8_t PINS) 
{
  HWREG(GPIO_PORTA_BASE+GPIO_O_DIR) &= ~PINS;
}

//************************************************************

/*Sets pins corresponding to high bits in "PINS" to
Digital.*/
void SetPin2Digital_PortB(uint8_t PINS)
{
  HWREG(GPIO_PORTB_BASE+GPIO_O_DEN) |= PINS;
}

/*Sets pins corresponding to high bits in "PINS" to
be output pins. A pin is an output if the corresponding
bit in the register is HIGH*/
void SetPin2Output_PortB(uint8_t PINS)
{
  HWREG(GPIO_PORTB_BASE+GPIO_O_DIR) |= PINS;
}

/*Sets pins corresponding to high bits in "PINS" to
be input pins. A pin is an input if the corresponding
bit in the register is LOW. */
void SetPin2Input_PortB(uint8_t PINS)
{
  HWREG(GPIO_PORTB_BASE+GPIO_O_DIR) &= ~PINS;
}

//************************************************************

/*Sets pins corresponding to high bits in "PINS" to
Digital.*/
void SetPin2Digital_PortD(uint8_t PINS)
{
  HWREG(GPIO_PORTD_BASE+GPIO_O_DEN) |= PINS;
}

/*Sets pins corresponding to high bits in "PINS" to
be output pins. A pin is an output if the corresponding
bit in the register is HIGH*/
void SetPin2Output_PortD(uint8_t PINS)
{
  HWREG(GPIO_PORTD_BASE+GPIO_O_DIR) |= PINS;
}

/*Sets pins corresponding to high bits in "PINS" to
be input pins. A pin is an input if the corresponding
bit in the register is LOW. */
void SetPin2Input_PortD(uint8_t PINS)
{
  HWREG(GPIO_PORTD_BASE+GPIO_O_DIR) &= ~PINS;
}
