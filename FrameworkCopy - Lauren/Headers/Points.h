/****************************************************************************

  Header file for Points.c
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef POINTS_H
#define POINTS_H

#include <stdint.h>
#include <stdbool.h>

#include "ES_Events.h"



// Public Function Prototypes

bool InitPoints(uint8_t Priority);
bool PostPoints(ES_Event_t ThisEvent);
ES_Event_t RunPoints(ES_Event_t ThisEvent);
uint32_t GetPoints(void);

#endif 